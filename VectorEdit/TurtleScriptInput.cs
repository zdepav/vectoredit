﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using VectorEdit.Turtle;

namespace VectorEdit
{
    internal partial class TurtleScriptInput : Form
    {
        Form1 parent;

        public TurtleScriptInput(Form1 f)
        {
            parent = f;
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            parent.shapes.AddRange(TurtleInterpreter.Interpret(textBox1.Text));
            Close();
        }
    }
}
