﻿using System.Windows.Forms;

namespace VectorEdit
{
    internal class ShapeContextMenu : ContextMenuStrip
    {
        public ShapeContextMenu(Shape s, params ShapeContextMenuItem[] items)
        {
            ToolStripMenuItem tsmi = new ToolStripMenuItem(s.ToString());
            tsmi.Enabled = false;
            Items.Add(tsmi);
            Items.AddRange(items);
            shape = s;
            ItemClicked += ShapeContextMenu_ItemClicked;
        }

        private Shape shape;

        private void ShapeContextMenu_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            ShapeContextMenuItem scmi = e.ClickedItem as ShapeContextMenuItem;
            Close();
            if (scmi != null) scmi.OnClicked(shape);
        }
    }
}