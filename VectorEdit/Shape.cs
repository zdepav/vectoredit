﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Windows.Forms;

namespace VectorEdit
{
    /// <summary>
    /// Generic Shape class
    /// </summary>
    internal abstract class Shape
    {
        /// <summary>
        /// Lower x coord
        /// </summary>
        public abstract int X { get; }
        /// <summary>
        /// Lower y coord
        /// </summary>
        public abstract int Y { get; }
        /// <summary>
        /// Higher x coord
        /// </summary>
        public abstract int X2 { get; }
        /// <summary>
        /// Higher y coord
        /// </summary>
        public abstract int Y2 { get; }

        /// <summary>
        /// Renders the shape
        /// </summary>
        /// <param name="g">Graphics to draw on</param>
        /// <param name="canvasTranslation">View translation vector</param>
        public abstract void Render(Graphics g, Point canvasTranslation);

        /// <summary>
        /// Renders the shape with all editing points and editing animation
        /// </summary>
        /// <param name="g">Graphics to draw on</param>
        /// <param name="canvasTranslation">View translation vector</param>
        /// <param name="time">Time for the animation (0 - 79)</param>
        public abstract void RenderEditing(Graphics g, Point canvasTranslation, int time);

        /// <summary>
        /// Computes positions of editing points
        /// </summary>
        /// <returns>Array of editing points</returns>
        public abstract Point[] EditingPoints();

        /// <summary>
        /// renders editing point
        /// </summary>
        /// <param name="g">Graphics to draw on</param>
        /// <param name="p">location of the editing point</param>
        public static void RenderEditingPoint(Graphics g, Point p)
        {
            g.FillRectangle(editingPointFillBrush, p.X - 1, p.Y - 1, 2, 2);
            g.DrawRectangle(editingPointBorderPen, p.X - 2, p.Y - 2, 4, 4);
        }

        /// <summary>
        /// Exports the shape to SVG element
        /// </summary>
        /// <param name="canvasTranslation">View translation vector</param>
        /// <returns>SVG reprezentation of this shape</returns>
        public abstract string RenderSVG(Point canvasTranslation);

        /// <summary>
        /// Saves this shape to binary stream
        /// </summary>
        /// <param name="bw">Binary stream for writing</param>
        public abstract void SaveBinary(BinaryWriter bw);

        /// <summary>
        /// Checks if point [x,y] is on this shape or near
        /// </summary>
        /// <param name="mx">X coord of checked point</param>
        /// <param name="my">Y coord of checked point</param>
        /// <param name="canvasTranslation">View translation vector</param>
        /// <returns>True if specified point "hits" this shape</returns>
        public abstract bool Hit(int mx, int my, Point canvasTranslation);
        
        public override string ToString()
        {
            return string.Format("Unknown Shape ([{0};{1}], [{2};{3}])", X, Y, X2, Y2);
        }

        public abstract void AttachPointP(int mx, int my, Point canvasTranslation, List<Point> points);

        public abstract void AttachPointS(int mx, int my, Point canvasTranslation, List<Point> points);

        public abstract void AttachPointToIntersection(int mx, int my, Shape s, Point canvasTranslation, List<Point> points);

        /// <summary>
        /// clearc colors and line width
        /// </summary>
        public abstract void ClearStyle();

        /// <summary>
        /// moves the whole shape
        /// </summary>
        /// <param name="vx">distance on x-axis</param>
        /// <param name="vy">distance on y-axis</param>
        public abstract void MoveAll(int vx, int vy);

        /// <summary>
        /// moves the specified point
        /// </summary>
        /// <param name="vx">distance on x-axis</param>
        /// <param name="vy">distance on y-axis</param>
        /// <param name="pointId">index of the moved point in EditingPoints</param>
        /// <returns>Tuple:
        ///     Item1: false if the shape should be deleted, true othervise
        ///     Item2: index of the created point
        /// </returns>
        public abstract Tuple<bool, int> MovePoint(int vx, int vy, int pointId);

        public static Pen preRenderPen, editingPointBorderPen;

        public static Brush editingPointFillBrush;

        protected static readonly int attachmentEpsylonSqr, attachmentEpsylon;

        public static readonly int editingPointMaxDistSqr;

        private struct TypeWithId
        {
            public Type T;
            public byte Id;

            public TypeWithId(Type t, byte id)
            {
                T = t;
                Id = id;
            }
        }

        private static TypeWithId[] types;

        static Shape()
        {
            preRenderPen = new Pen(new HatchBrush(HatchStyle.LargeCheckerBoard, Color.Blue, Color.Transparent));
            editingPointBorderPen = new Pen(new HatchBrush(HatchStyle.Percent50, Color.Black, Color.FromArgb(96, 96, 96)));
            editingPointFillBrush = new HatchBrush(HatchStyle.Percent50, Color.FromArgb(160, 160, 160), Color.White);
            types = new TypeWithId[] { new TypeWithId(typeof(Line), 1), new TypeWithId(typeof(Rect), 2), new TypeWithId(typeof(Ellipse), 3)};
            attachmentEpsylonSqr = 25;
            attachmentEpsylon = 5;
            editingPointMaxDistSqr = 25;
        }

        public static byte ShapeToId(Type type)
        {
            foreach (TypeWithId twid in types)
            {
                if (twid.T == type) return twid.Id;
            }
            return 0;
        }

        public static Type ShapeFromId(byte id)
        {
            foreach (TypeWithId twid in types)
            {
                if (twid.Id == id) return twid.T;
            }
            return null;
        }

        public abstract Line[] ToLines();
    }
}
