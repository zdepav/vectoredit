﻿using System;
using System.Windows.Forms;

namespace VectorEdit
{
    internal class ShapeContextMenuItem : ToolStripMenuItem
    {
        public delegate void ShapeContextMenuItemClick(Shape s);

        public ShapeContextMenuItem(string text, ShapeContextMenuItemClick onClick) : base(text)
        {
            this.text = text;
            this.onClick = onClick;
        }

        private ShapeContextMenuItemClick onClick;

        private string text;

        public void OnClicked(Shape shape)
        {
            if (onClick != null)
                onClick.Invoke(shape);
        }
    }

    internal class ShapeContextMenuDivider : ShapeContextMenuItem
    {
        public ShapeContextMenuDivider() : base("", null)
        {
            Enabled = false;
        }
    }
}