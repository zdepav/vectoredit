﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VectorEdit.EditDialogs
{
    internal partial class LineEditor : Form,  ShapeEditor
    {
        Line line;
        Form1 form;
        ColorPicker cp;

        public LineEditor(Line l, Form1 f)
        {
            line = l;
            form = f;
            cp = new ColorPicker();
            cp.EnableAlpha = false;
            InitializeComponent();
        }

        private void LineEditor_Load(object sender, EventArgs e)
        {
            _Refresh();
        }

        private void coordX1_ValueChanged(object sender, EventArgs e)
        {
            line.x = (int)coordX1.Value;
            preview.Refresh();
        }

        private void coordY1_ValueChanged(object sender, EventArgs e)
        {
            line.y = (int)coordY1.Value;
            preview.Refresh();
        }

        private void coordX2_ValueChanged(object sender, EventArgs e)
        {
            line.x2 = (int)coordX2.Value;
            preview.Refresh();
        }

        private void coordY2_ValueChanged(object sender, EventArgs e)
        {
            line.y2 = (int)coordY2.Value;
            preview.Refresh();
        }

        private void width_ValueChanged(object sender, EventArgs e)
        {
            line.width = (byte)width.Value;
            preview.Refresh();
        }

        private void color_Click(object sender, EventArgs e)
        {
            Color c = cp.Color;
            if (cp.ShowDialog() == DialogResult.OK)
            {
                line.color = color.BackColor = cp.Color;
                color.Refresh();
                preview.Refresh();
            }
            else cp.Color = c;
        }

        private void preview_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.Clear(Color.White);
            e.Graphics.SmoothingMode = SmoothingMode.AntiAlias;
            e.Graphics.DrawLine(new Pen(color.BackColor, (float)width.Value), 32, 16, 128, 112);
        }

        private void LineEditor_FormClosed(object sender, FormClosedEventArgs e)
        {
            form.RemoveShapeEditor(line);
        }

        public void _Focus() { Focus(); }

        public void _Show() { Show(); }

        public void _Close() { Close(); }

        public bool _Shown() { return Visible; }

        public void _Refresh()
        {
            coordX1.Value = line.x;
            coordY1.Value = line.y;
            coordX2.Value = line.x2;
            coordY2.Value = line.y2;
            width.Value = line.width;
            cp.Color = color.BackColor = line.color;
            color.Refresh();
            preview.Refresh();
        }

        private void color_Paint(object sender, PaintEventArgs e)
        {
            for (int i = 0; i < color.Width; i += 12)
            {
                for (int j = 0; j < color.Height; j += 12)
                {
                    e.Graphics.DrawImage(Resource1.alpha_back, i, j);
                }
            }
            e.Graphics.FillRectangle(new SolidBrush(color.BackColor), 0, 0, color.Width, color.Height);
        }
    }
}
