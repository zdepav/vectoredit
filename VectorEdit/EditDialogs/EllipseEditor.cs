﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VectorEdit.EditDialogs
{
    internal partial class EllipseEditor : Form, ShapeEditor
    {
        Ellipse ellipse;
        Form1 form;
        ColorPicker cp;

        public EllipseEditor(Ellipse e, Form1 f)
        {
            ellipse = e;
            form = f;
            cp = new ColorPicker();
            InitializeComponent();
        }

        private void EllipseEditor_Load(object sender, EventArgs e)
        {
            _Refresh();
        }

        public void _Refresh()
        {
            coordX.Value = ellipse.cx;
            coordY.Value = ellipse.cy;
            width.Value = ellipse.a;
            height.Value = ellipse.b;
            lineWidth.Value = ellipse.lineWidth;
            fillColor.BackColor = ellipse.fillColor;
            lineColor.BackColor = ellipse.lineColor;
            fillColor.Refresh();
            lineColor.Refresh();
            preview.Refresh();
        }

        private void preview_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.Clear(Color.White);
            e.Graphics.SmoothingMode = SmoothingMode.AntiAlias;
            e.Graphics.FillEllipse(new SolidBrush(fillColor.BackColor), 16, 32, 128, 64);
            e.Graphics.DrawEllipse(new Pen(lineColor.BackColor, (float)lineWidth.Value), 16, 32, 128, 64);
        }

        private void EllipseEditor_FormClosed(object sender, FormClosedEventArgs e)
        {
            form.RemoveShapeEditor(ellipse);
        }

        public void _Focus() { Focus(); }

        public void _Show() { Show(); }

        public void _Close() { Close(); }

        public bool _Shown() { return Visible; }

        private void coordCX_ValueChanged(object sender, EventArgs e)
        {
            ellipse.cx = (int)coordX.Value;
            preview.Refresh();
        }

        private void coordCY_ValueChanged(object sender, EventArgs e)
        {
            ellipse.cy = (int)coordY.Value;
            preview.Refresh();
        }

        private void a_ValueChanged(object sender, EventArgs e)
        {
            ellipse.a = (int)width.Value;
            preview.Refresh();
        }

        private void b_ValueChanged(object sender, EventArgs e)
        {
            ellipse.b = (int)height.Value;
            preview.Refresh();
        }

        private void lineWidth_ValueChanged(object sender, EventArgs e)
        {
            ellipse.lineWidth = (byte)lineWidth.Value;
            preview.Refresh();
        }

        private void fillColor_Click(object sender, EventArgs e)
        {
            cp.EnableAlpha = true;
            Color c = cp.Color = ellipse.fillColor;
            if (cp.ShowDialog() == DialogResult.OK)
            {
                ellipse.fillColor = fillColor.BackColor = cp.Color;
                fillColor.Refresh();
                preview.Refresh();
            }
            else cp.Color = c;
        }

        private void lineColor_Click(object sender, EventArgs e)
        {
            cp.EnableAlpha = false;
            Color c = cp.Color = ellipse.lineColor;
            if (cp.ShowDialog() == DialogResult.OK)
            {
                ellipse.lineColor = lineColor.BackColor = cp.Color;
                lineColor.Refresh();
                preview.Refresh();
            }
            else cp.Color = c;
        }

        private void fillColor_Paint(object sender, PaintEventArgs e)
        {
            for (int i = 0; i < fillColor.Width; i += 12)
            {
                for (int j = 0; j < fillColor.Height; j += 12)
                {
                    e.Graphics.DrawImage(Resource1.alpha_back, i, j);
                }
            }
            e.Graphics.FillRectangle(new SolidBrush(fillColor.BackColor), 0, 0, fillColor.Width, fillColor.Height);
        }

        private void lineColor_Paint(object sender, PaintEventArgs e)
        {
            for (int i = 0; i < lineColor.Width; i += 12)
            {
                for (int j = 0; j < lineColor.Height; j += 12)
                {
                    e.Graphics.DrawImage(Resource1.alpha_back, i, j);
                }
            }
            e.Graphics.FillRectangle(new SolidBrush(lineColor.BackColor), 0, 0, lineColor.Width, lineColor.Height);
        }
    }
}
