﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VectorEdit.EditDialogs
{
    internal partial class RectEditor : Form, ShapeEditor
    {
        Rect rect;
        Form1 form;
        ColorPicker cp;

        public RectEditor(Rect r, Form1 f)
        {
            rect = r;
            form = f;
            cp = new ColorPicker();
            InitializeComponent();
        }

        private void RectEditor_Load(object sender, EventArgs e)
        {
            _Refresh();
        }

        public void _Refresh()
        {
            coordX.Value = rect.X;
            coordY.Value = rect.Y;
            width.Value = rect.w;
            height.Value = rect.h;
            lineWidth.Value = rect.lineWidth;
            fillColor.BackColor = rect.fillColor;
            lineColor.BackColor = rect.lineColor;
            fillColor.Refresh();
            lineColor.Refresh();
            preview.Refresh();
        }

        private void preview_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.Clear(Color.White);
            e.Graphics.SmoothingMode = SmoothingMode.AntiAlias;
            e.Graphics.FillRectangle(new SolidBrush(fillColor.BackColor), 16, 32, 128, 64);
            e.Graphics.DrawRectangle(new Pen(lineColor.BackColor, (float)lineWidth.Value), 16, 32, 128, 64);
        }

        private void RectEditor_FormClosed(object sender, FormClosedEventArgs e)
        {
            form.RemoveShapeEditor(rect);
        }

        public void _Focus() { Focus(); }

        public void _Show() { Show(); }

        public bool _Shown() { return Visible; }

        public void _Close() { Close(); }

        private void coordX_ValueChanged(object sender, EventArgs e)
        {
            rect.x = (int)coordX.Value;
            preview.Refresh();
        }

        private void coordY_ValueChanged(object sender, EventArgs e)
        {
            rect.y = (int)coordY.Value;
            preview.Refresh();
        }

        private void width_ValueChanged(object sender, EventArgs e)
        {
            rect.w = (int)width.Value;
            preview.Refresh();
        }

        private void height_ValueChanged(object sender, EventArgs e)
        {
            rect.h = (int)height.Value;
            preview.Refresh();
        }

        private void lineWidth_ValueChanged(object sender, EventArgs e)
        {
            rect.lineWidth = (byte)lineWidth.Value;
            preview.Refresh();
        }

        private void fillColor_Click(object sender, EventArgs e)
        {
            cp.EnableAlpha = true;
            Color c = cp.Color = rect.fillColor;
            if (cp.ShowDialog() == DialogResult.OK)
            {
                rect.fillColor = fillColor.BackColor = cp.Color;
                fillColor.Refresh();
                preview.Refresh();
            }
            else cp.Color = c;
        }

        private void lineColor_Click(object sender, EventArgs e)
        {
            cp.EnableAlpha = false;
            Color c = cp.Color = rect.lineColor;
            if (cp.ShowDialog() == DialogResult.OK)
            {
                rect.lineColor = lineColor.BackColor = cp.Color;
                lineColor.Refresh();
                preview.Refresh();
            }
            else cp.Color = c;
        }

        private void fillColor_Paint(object sender, PaintEventArgs e)
        {
            for (int i = 0; i < fillColor.Width; i += 12)
            {
                for (int j = 0; j < fillColor.Height; j += 12)
                {
                    e.Graphics.DrawImage(Resource1.alpha_back, i, j);
                }
            }
            e.Graphics.FillRectangle(new SolidBrush(fillColor.BackColor), 0, 0, fillColor.Width, fillColor.Height);
        }

        private void lineColor_Paint(object sender, PaintEventArgs e)
        {
            for (int i = 0; i < lineColor.Width; i += 12)
            {
                for (int j = 0; j < lineColor.Height; j += 12)
                {
                    e.Graphics.DrawImage(Resource1.alpha_back, i, j);
                }
            }
            e.Graphics.FillRectangle(new SolidBrush(lineColor.BackColor), 0, 0, lineColor.Width, lineColor.Height);
        }
    }
}
