﻿namespace VectorEdit.EditDialogs
{
    partial class RectEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.preview = new System.Windows.Forms.PictureBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.height = new System.Windows.Forms.NumericUpDown();
            this.width = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.fillColor = new System.Windows.Forms.PictureBox();
            this.lineWidth = new System.Windows.Forms.NumericUpDown();
            this.coordY = new System.Windows.Forms.NumericUpDown();
            this.coordX = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lineColor = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.preview)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.height)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.width)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fillColor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lineWidth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.coordY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.coordX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lineColor)).BeginInit();
            this.SuspendLayout();
            // 
            // preview
            // 
            this.preview.Location = new System.Drawing.Point(215, 35);
            this.preview.Name = "preview";
            this.preview.Size = new System.Drawing.Size(160, 128);
            this.preview.TabIndex = 28;
            this.preview.TabStop = false;
            this.preview.Paint += new System.Windows.Forms.PaintEventHandler(this.preview_Paint);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(21, 145);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(49, 13);
            this.label6.TabIndex = 27;
            this.label6.Text = "Fill Color:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(9, 118);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 13);
            this.label5.TabIndex = 26;
            this.label5.Text = "Line Width:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(29, 92);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 13);
            this.label3.TabIndex = 25;
            this.label3.Text = "Height:";
            // 
            // height
            // 
            this.height.Location = new System.Drawing.Point(76, 90);
            this.height.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.height.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.height.Name = "height";
            this.height.Size = new System.Drawing.Size(120, 20);
            this.height.TabIndex = 24;
            this.height.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.height.ValueChanged += new System.EventHandler(this.height_ValueChanged);
            // 
            // width
            // 
            this.width.Location = new System.Drawing.Point(76, 64);
            this.width.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.width.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.width.Name = "width";
            this.width.Size = new System.Drawing.Size(120, 20);
            this.width.TabIndex = 23;
            this.width.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.width.ValueChanged += new System.EventHandler(this.width_ValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(32, 66);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(38, 13);
            this.label4.TabIndex = 22;
            this.label4.Text = "Width:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(53, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(17, 13);
            this.label2.TabIndex = 21;
            this.label2.Text = "Y:";
            // 
            // fillColor
            // 
            this.fillColor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.fillColor.Location = new System.Drawing.Point(76, 142);
            this.fillColor.Name = "fillColor";
            this.fillColor.Size = new System.Drawing.Size(120, 20);
            this.fillColor.TabIndex = 20;
            this.fillColor.TabStop = false;
            this.fillColor.Click += new System.EventHandler(this.fillColor_Click);
            this.fillColor.Paint += new System.Windows.Forms.PaintEventHandler(this.fillColor_Paint);
            // 
            // lineWidth
            // 
            this.lineWidth.Location = new System.Drawing.Point(76, 116);
            this.lineWidth.Maximum = new decimal(new int[] {
            128,
            0,
            0,
            0});
            this.lineWidth.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.lineWidth.Name = "lineWidth";
            this.lineWidth.Size = new System.Drawing.Size(120, 20);
            this.lineWidth.TabIndex = 19;
            this.lineWidth.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.lineWidth.ValueChanged += new System.EventHandler(this.lineWidth_ValueChanged);
            // 
            // coordY
            // 
            this.coordY.Location = new System.Drawing.Point(76, 38);
            this.coordY.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.coordY.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.coordY.Name = "coordY";
            this.coordY.Size = new System.Drawing.Size(120, 20);
            this.coordY.TabIndex = 18;
            this.coordY.ValueChanged += new System.EventHandler(this.coordY_ValueChanged);
            // 
            // coordX
            // 
            this.coordX.Location = new System.Drawing.Point(76, 12);
            this.coordX.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.coordX.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.coordX.Name = "coordX";
            this.coordX.Size = new System.Drawing.Size(120, 20);
            this.coordX.TabIndex = 17;
            this.coordX.ValueChanged += new System.EventHandler(this.coordX_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(53, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(17, 13);
            this.label1.TabIndex = 16;
            this.label1.Text = "X:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(13, 172);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(57, 13);
            this.label7.TabIndex = 30;
            this.label7.Text = "Line Color:";
            // 
            // lineColor
            // 
            this.lineColor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lineColor.Location = new System.Drawing.Point(76, 168);
            this.lineColor.Name = "lineColor";
            this.lineColor.Size = new System.Drawing.Size(120, 20);
            this.lineColor.TabIndex = 29;
            this.lineColor.TabStop = false;
            this.lineColor.Click += new System.EventHandler(this.lineColor_Click);
            this.lineColor.Paint += new System.Windows.Forms.PaintEventHandler(this.lineColor_Paint);
            // 
            // RectEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(389, 199);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.lineColor);
            this.Controls.Add(this.preview);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.height);
            this.Controls.Add(this.width);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.fillColor);
            this.Controls.Add(this.lineWidth);
            this.Controls.Add(this.coordY);
            this.Controls.Add(this.coordX);
            this.Controls.Add(this.label1);
            this.Name = "RectEditor";
            this.Text = "Rectangle Editor";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.RectEditor_FormClosed);
            this.Load += new System.EventHandler(this.RectEditor_Load);
            ((System.ComponentModel.ISupportInitialize)(this.preview)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.height)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.width)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fillColor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lineWidth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.coordY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.coordX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lineColor)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox preview;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown height;
        private System.Windows.Forms.NumericUpDown width;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox fillColor;
        private System.Windows.Forms.NumericUpDown lineWidth;
        private System.Windows.Forms.NumericUpDown coordY;
        private System.Windows.Forms.NumericUpDown coordX;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.PictureBox lineColor;
    }
}