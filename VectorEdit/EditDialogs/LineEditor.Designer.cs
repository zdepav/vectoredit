﻿namespace VectorEdit.EditDialogs
{
    partial class LineEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.coordX1 = new System.Windows.Forms.NumericUpDown();
            this.coordY1 = new System.Windows.Forms.NumericUpDown();
            this.width = new System.Windows.Forms.NumericUpDown();
            this.color = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.coordY2 = new System.Windows.Forms.NumericUpDown();
            this.coordX2 = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.preview = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.coordX1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.coordY1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.width)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.color)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.coordY2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.coordX2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.preview)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(27, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(23, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "X1:";
            // 
            // coordX1
            // 
            this.coordX1.Location = new System.Drawing.Point(56, 7);
            this.coordX1.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.coordX1.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.coordX1.Name = "coordX1";
            this.coordX1.Size = new System.Drawing.Size(120, 20);
            this.coordX1.TabIndex = 1;
            this.coordX1.ValueChanged += new System.EventHandler(this.coordX1_ValueChanged);
            // 
            // coordY1
            // 
            this.coordY1.Location = new System.Drawing.Point(56, 33);
            this.coordY1.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.coordY1.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.coordY1.Name = "coordY1";
            this.coordY1.Size = new System.Drawing.Size(120, 20);
            this.coordY1.TabIndex = 3;
            this.coordY1.ValueChanged += new System.EventHandler(this.coordY1_ValueChanged);
            // 
            // width
            // 
            this.width.Location = new System.Drawing.Point(56, 111);
            this.width.Maximum = new decimal(new int[] {
            128,
            0,
            0,
            0});
            this.width.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.width.Name = "width";
            this.width.Size = new System.Drawing.Size(120, 20);
            this.width.TabIndex = 4;
            this.width.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.width.ValueChanged += new System.EventHandler(this.width_ValueChanged);
            // 
            // color
            // 
            this.color.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.color.Location = new System.Drawing.Point(56, 137);
            this.color.Name = "color";
            this.color.Size = new System.Drawing.Size(120, 20);
            this.color.TabIndex = 6;
            this.color.TabStop = false;
            this.color.Click += new System.EventHandler(this.color_Click);
            this.color.Paint += new System.Windows.Forms.PaintEventHandler(this.color_Paint);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(27, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(23, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Y1:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(27, 87);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(23, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "Y2:";
            // 
            // coordY2
            // 
            this.coordY2.Location = new System.Drawing.Point(56, 85);
            this.coordY2.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.coordY2.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.coordY2.Name = "coordY2";
            this.coordY2.Size = new System.Drawing.Size(120, 20);
            this.coordY2.TabIndex = 11;
            this.coordY2.ValueChanged += new System.EventHandler(this.coordY2_ValueChanged);
            // 
            // coordX2
            // 
            this.coordX2.Location = new System.Drawing.Point(56, 59);
            this.coordX2.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.coordX2.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.coordX2.Name = "coordX2";
            this.coordX2.Size = new System.Drawing.Size(120, 20);
            this.coordX2.TabIndex = 10;
            this.coordX2.ValueChanged += new System.EventHandler(this.coordX2_ValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(27, 61);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(23, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "X2:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 113);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(38, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "Width:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 141);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(34, 13);
            this.label6.TabIndex = 14;
            this.label6.Text = "Color:";
            // 
            // preview
            // 
            this.preview.Location = new System.Drawing.Point(195, 17);
            this.preview.Name = "preview";
            this.preview.Size = new System.Drawing.Size(160, 128);
            this.preview.TabIndex = 15;
            this.preview.TabStop = false;
            this.preview.Paint += new System.Windows.Forms.PaintEventHandler(this.preview_Paint);
            // 
            // LineEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(371, 168);
            this.Controls.Add(this.preview);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.coordY2);
            this.Controls.Add(this.coordX2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.color);
            this.Controls.Add(this.width);
            this.Controls.Add(this.coordY1);
            this.Controls.Add(this.coordX1);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "LineEditor";
            this.Text = "Line Editor";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.LineEditor_FormClosed);
            this.Load += new System.EventHandler(this.LineEditor_Load);
            ((System.ComponentModel.ISupportInitialize)(this.coordX1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.coordY1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.width)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.color)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.coordY2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.coordX2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.preview)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown coordX1;
        private System.Windows.Forms.NumericUpDown coordY1;
        private System.Windows.Forms.NumericUpDown width;
        private System.Windows.Forms.PictureBox color;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown coordY2;
        private System.Windows.Forms.NumericUpDown coordX2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.PictureBox preview;
    }
}