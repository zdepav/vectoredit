﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VectorEdit
{
    /// <summary>
    /// Line shape class
    /// </summary>
    internal class Line : Shape
    {
        public int x { get; set; }
        public int y { get; set; }
        public int x2 { get; set; }
        public int y2 { get; set; }
        public override int X { get { return Math.Min(x, x2); } }
        public override int Y { get { return Math.Min(y, y2); } }
        public override int X2 { get { return Math.Max(x, x2); } }
        public override int Y2 { get { return Math.Max(y, y2); } }

        public Color color;

        public byte width;
        byte width_d2 { get { return (byte)(width / 2); } }

        public Line(int x1, int y1, int x2, int y2)
        {
            x = x1;
            this.x2 = x2;
            y = y1;
            this.y2 = y2;
            this.color = Color.Black;
            this.width = 1;
        }

        public Line(int x1, int y1, int x2, int y2, Color color, byte width = 2)
        {
            x = x1;
            this.x2 = x2;
            y = y1;
            this.y2 = y2;
            this.color = color;
            this.width = width;
        }

        public Line(BinaryReader br)
        {
            x = br.ReadInt32();
            y = br.ReadInt32();
            x2 = br.ReadInt32();
            y2 = br.ReadInt32();
            color = Color.FromArgb(br.ReadByte(), br.ReadByte(), br.ReadByte());
            width = br.ReadByte();
        }

        public override void Render(Graphics g, Point canvasTranslation)
        {
            g.DrawLine(new Pen(color, width),
                x + canvasTranslation.X, y + canvasTranslation.Y,
                x2 + canvasTranslation.X, y2 + canvasTranslation.Y);
        }

        public override void RenderEditing(Graphics g, Point canvasTranslation, int time)
        {
            int _x = x + canvasTranslation.X, _y = y + canvasTranslation.Y;
            int _x2 = x2 + canvasTranslation.X, _y2 = y2 + canvasTranslation.Y;
            int a = (int)Math.Round(255 * ((1 + Math.Cos(Math.PI * time / 40.0)) / 2));
            g.DrawLine(new Pen(Color.FromArgb(a, color.R, color.G, color.B), width), _x, _y, _x2, _y2);
            g.DrawLine(new Pen(new HatchBrush(HatchStyle.LargeCheckerBoard, Color.FromArgb(255 - a, 0, 0, 255), Color.Transparent)), _x, _y, _x2, _y2);
            RenderEditingPoint(g, new Point(_x, _y));
            RenderEditingPoint(g, new Point(_x2, _y2));
        }

        public override Point[] EditingPoints()
        {
            return new Point[] { new Point(x, y), new Point(x2, y2) };
        }

        public override string RenderSVG(Point canvasTranslation)
        {
            return string.Format("<line x1=\"{0}\" y1=\"{1}\" x2=\"{2}\" y2=\"{3}\" style=\"stroke: #{4:x2}{5:x2}{6:x2}; stroke-width: {7}px;\" />",
                x + canvasTranslation.X, y + canvasTranslation.Y, x2 + canvasTranslation.X, y2 + canvasTranslation.Y, color.R, color.G, color.B, width);
        }

        public override void SaveBinary(BinaryWriter bw)
        {
            bw.Write(ShapeToId(GetType()));
            bw.WriteR(x).WriteR(y).WriteR(x2).Write(y2);
            bw.WriteR(color.R).WriteR(color.G).Write(color.B);
            bw.Write(width);
        }

        public static void PreRender(Graphics g, int x1, int y1, int x2, int y2)
        {
            g.DrawLine(preRenderPen, x1, y1, x2, y2);
        }

        public override bool Hit(int mx, int my, Point canvasTranslation)
        {
            mx -= canvasTranslation.X;
            my -= canvasTranslation.Y;
            double a = y - y2;
            double b = x2 - x;
            double c = -a * x - b * y;
            double d = Math.Abs(a * mx + b * my + c) / Math.Sqrt(a * a + b * b);
            return ((d < width_d2 + 2) && (Math.Abs(x2 - x) > Math.Abs(y2 - y) ? ((mx > Math.Min(x, x2)) && (mx < Math.Max(x, x2))) : ((my > Math.Min(y, y2)) && (my < Math.Max(y, y2)))));
        }

        public override void AttachPointP(int mx, int my, Point canvasTranslation, List<Point> points)
        {
            mx -= canvasTranslation.X;
            my -= canvasTranslation.Y;
            if (((x - mx) * (x - mx) + (y - my) * (y - my)) < attachmentEpsylonSqr)
                points.Add(new Point(x + canvasTranslation.X, y + canvasTranslation.Y));
            if (((x2 - mx) * (x2 - mx) + (y2 - my) * (y2 - my)) < attachmentEpsylonSqr)
                points.Add(new Point(x2 + canvasTranslation.X, y2 + canvasTranslation.Y));
        }

        public override void AttachPointS(int mx, int my, Point canvasTranslation, List<Point> points)
        {
            mx -= canvasTranslation.X;
            my -= canvasTranslation.Y;
            double a = y - y2, b = x2 - x, c = -a * x - b * y;
            double d = Math.Abs(a * mx + b * my + c) / Math.Sqrt(a * a + b * b);
            if ((d < attachmentEpsylon) && ((Math.Abs(x2 - x) > Math.Abs(y2 - y)) ? ((mx > Math.Min(x, x2)) && (mx < Math.Max(x, x2))) : ((my > Math.Min(y, y2)) && (my < Math.Max(y, y2)))))
            {
                double p = x2 - x, q = y2 - y, r = -p * mx - q * my;
                points.Add(new Point((int)Math.Round((c * q - b * r) / (b * p - a * q)) + canvasTranslation.X,
                    (int)Math.Round((c * p - a * r) / (a * q - b * p)) + canvasTranslation.Y));
            }
        }

        private Point? IntersectionH(int _x, int _y, int _x2)
        {
            double vx = x2 - x, vy = y2 - y, va = _x2 - _x;
            double z = vy * va;
            if (Util.Cmp(z, 0)) return null;
            double u = (vy * (x - _x) - vx * (y - _y)) / z;
            double t;
            if (vx != 0)
                t = (_x + va * u - x) / vx;
            else
                t = (_y - y) / vy;
            if (u >= 0 && u <= 1 && t >= 0 && t <= 1)
                return new Point((int)Math.Round(_x + va * u), _y);
            return null;
        }

        private Point? IntersectionV(int _x, int _y, int _y2)
        {
            double vx = x2 - x, vy = y2 - y, vb = _y2 - _y;
            double z = - vx * vb;
            if (Util.Cmp(z, 0)) return null;
            double u = (vy * (x - _x) - vx * (y - _y)) / z;
            double t;
            if (vx != 0)
                t = (_x - x) / vx;
            else
                t = (_y + vb * u - y) / vy;
            if (u >= 0 && u <= 1 && t >= 0 && t <= 1)
                return new Point(_x, (int)Math.Round(_y + vb * u));
            return null;
        }

        public static Point? Intersection(PointF l1_1, PointF l1_2, PointF l2_1, PointF l2_2)
        {
            double vx = l1_2.X - l1_1.X, vy = l1_2.Y - l1_1.Y, va = l2_2.X - l2_1.X, vb = l2_2.Y - l2_1.Y;
            double z = vy * va - vx * vb;
            if (Util.Cmp(z, 0)) return null;
            double u = (vy * (l1_1.X - l2_1.X) - vx * (l1_1.Y - l2_1.Y)) / z;
            double t;
            if (vx != 0)
                t = (l2_1.X + va * u - l1_1.X) / vx;
            else
                t = (l2_1.Y + vb * u - l1_1.Y) / vy;
            if (u >= 0 && u <= 1 && t >= 0 && t <= 1)
            {
                return new Point((int)Math.Round(l2_1.X + va * u), (int)Math.Round(l2_1.Y + vb * u));
            }
            return null;
        }

        public override void AttachPointToIntersection(int mx, int my, Shape s, Point canvasTranslation, List<Point> points)
        {
            mx -= canvasTranslation.X;
            my -= canvasTranslation.Y;
            if (s is Line)
            {
                Line l = (Line)s;
                double a = l.x, a2 = l.x2, b = l.y, b2 = l.y2;
                double vx = x2 - x, vy = y2 - y, va = a2 - a, vb = b2 - b;
                double z = vy * va - vx * vb;
                if (Util.Cmp(z, 0)) return;
                double u = (vy * (x - a) - vx * (y - b)) / z;
                double t;
                if (vx != 0)
                    t = (a + va * u - x) / vx;
                else
                    t = (b + vb * u - y) / vy;
                if (u >= 0 && u <= 1 && t >= 0 && t <= 1)
                {
                    Point p = new Point((int)Math.Round(a + va * u),
                        (int)Math.Round(b + vb * u));
                    if (Util.DistSqr(p.X, p.Y, mx, my) < attachmentEpsylonSqr)
                        points.Add(new Point(p.X + canvasTranslation.X, p.Y + canvasTranslation.Y));
                }
            }
            else if (s is Rect)
            {
                Rect r = (Rect)s;
                int rx = r.X, ry = r.Y, rx2 = r.X2, ry2 = r.Y2;
                Point?[] ps = new Point?[4];
                ps[0] = IntersectionH(rx, ry, rx2);
                ps[1] = IntersectionV(rx2, ry, ry2);
                ps[2] = IntersectionH(rx, ry2, rx2);
                ps[3] = IntersectionV(rx, ry, ry2);
                foreach (Point? p in ps)
                    if (p.HasValue && DistSqr(p.Value, mx, my) < attachmentEpsylonSqr)
                        points.Add(new Point(p.Value.X + canvasTranslation.X, p.Value.Y + canvasTranslation.Y));
            }
            else if(s is Ellipse)
            {
                Ellipse e = (Ellipse)s;
                PointF[] pts = e.ToPolyline(72);
                PointF A = new PointF(x, y), B = new PointF(x2, y2);
                Point? pt;
                Point p;
                for (int i = 0; i < 72; i++)
                {
                    pt = Intersection(A, B, pts[i], pts[(i + 1) % 72]);
                    if (pt.HasValue) {
                        p = pt.Value;
                        p = e.MoveToEdge(p.X, p.Y);
                        if (Util.DistSqr(p.X, p.Y, mx, my) < attachmentEpsylonSqr)
                            points.Add(new Point(p.X + canvasTranslation.X, p.Y + canvasTranslation.Y));
                    }
                }
            }
        }

        private int DistSqr(Point p, int mx, int my)
        {
            return ((p.X - mx) * (p.X - mx) + (p.Y - my) * (p.Y - my));
        }

        public override string ToString()
        {
            return string.Format("Line from [{0};{1}] to [{2};{3}] with length of {4}", x, y, x2, y2, Math.Round(Util.Dist(x, y, x2, y2), 2));
        }

        public override void ClearStyle()
        {
            color = Color.Black;
            width = 2;
        }

        public override void MoveAll(int vx, int vy)
        {
            x += vx;
            y += vy;
            x2 += vx;
            y2 += vy;
        }

        public override Tuple<bool, int> MovePoint(int vx, int vy, int pointId)
        {
            if (pointId == 0)
            {
                x += vx;
                y += vy;
            }
            else if (pointId == 1)
            {
                x2 += vx;
                y2 += vy;
            }
            return new Tuple<bool, int>(Util.DistSqr(x, y, x2, y2) > 4, pointId);
        }

        public override Line[] ToLines()
        {
            return new Line[] { this };
        }
    }
}
