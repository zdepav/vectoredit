﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using VectorEdit.EditDialogs;
using static System.Environment;

namespace VectorEdit
{
    internal partial class Form1 : Form
    {
        private enum Tools { Line, Rect, Ellipse }
        private enum Actions { Draw, Edit, Move, MoveCanvas }

        private int mouseClickX, mouseClickY, mouseX, mouseY;
        private bool creatingShape { get { return mousePressed && action == Actions.Draw; } }
        private bool movingCanvas { get { return mousePressed && action == Actions.MoveCanvas; } }
        private bool mousePressed, mouseMPressed;

        private ShapeContextMenuItem[] shapeContextMenuItems;

        private int canvasTranslationX, canvasTranslationY;
        private Point canvasTranslation { get { return new Point(canvasTranslationX, canvasTranslationY); } }

        private static Pen gridPen = new Pen(new HatchBrush(HatchStyle.LargeCheckerBoard, Color.FromArgb(128, 64, 64, 64), Color.Transparent));

        private Tools tool;
        private Actions action;

        private ImageAttributes grayscale;

        private ColorPicker hslColorPicker;

        private HashSet<Keys> pressedKeys;

        private List<Shape> editing;
        public List<Shape> shapes;
        private Shape movingShape;
        private List<int> movingPointIds;
        private int editingAnimationTime;

        private Dictionary<Shape, ShapeEditor> shapeEditors;

        private bool _saved;
        private bool saved
        {
            get
            {
                return _saved;
            }
            set
            {
                _saved = value;
                Text = GenText();
            }
        }

        private string _fileName;
        private string fileName
        {
            get
            {
                return _fileName;
            }
            set
            {
                _fileName = value;
                Text = GenText();
            }
        }

        private string GenText()
        {
            string n;
            if (fileName == null)
                n = "New";
            else n = fileName;
            if (saved) return "VectorEdit - " + n;
            else return "VectorEdit - *" + n;
        }

        public void RemoveShapeEditor(Shape s)
        {
            shapeEditors.Remove(s);
        }

        private Rectangle CanvasSize
        {
            get
            {
                if (shapes.Count == 0)
                    return new Rectangle(0, 0, 1, 1);
                Shape s = shapes[0];
                if (shapes.Count == 1)
                {
                    return new Rectangle(s.X - 8, s.Y - 8, s.X2 - s.X + 16, s.Y2 - s.Y + 16);
                }
                int x1 = s.X, x2 = s.X2, y1 = s.Y, y2 = s.Y2;
                for (int i = 1; i < shapes.Count; i++)
                {
                    s = shapes[i];
                    if (s.X < x1) x1 = s.X;
                    if (s.Y < y1) y1 = s.Y;
                    if (s.X2 > x2) x2 = s.X2;
                    if (s.Y2 > y2) y2 = s.Y2;
                }
                return new Rectangle(x1 - 8, y1 - 8, x2 - x1 + 16, y2 - y1 + 16);
            }
        }

        private static string
            vecFileDialogFilter = "Scalable Vector Graphics|*.svg|Binary file|*.vbd",
            imgFileDialogFilter = "PNG file|*.png|JPEG file|*.jpg";

        public Form1()
        {
            shapes = new List<Shape>();
            grayscale = new ImageAttributes();
            editing = new List<Shape>();
            movingShape = null;
            movingPointIds = new List<int>();
            editingAnimationTime = 0;
            shapeEditors = new Dictionary<Shape, ShapeEditor>();
            pressedKeys = new HashSet<Keys>();
            grayscale.SetColorMatrix(new ColorMatrix(new float[][] {
                new float[] {0.3f , 0.3f , 0.3f , 0    , 0},
                new float[] {0.59f, 0.59f, 0.59f, 0    , 0},
                new float[] {0.11f, 0.11f, 0.11f, 0    , 0},
                new float[] {0    , 0    , 0    , 0.65f, 0},
                new float[] {0    , 0    , 0    , 0    , 1}}));
            mouseClickX = mouseClickY = mouseX = mouseY = -1;
            mousePressed = mouseMPressed = false;
            InitializeComponent();
            saved = true;
            fileName = null;
            hslColorPicker = new ColorPicker();
            hslColorPicker.Color = Color.Black;
            pbColor.Refresh();
            canvasTranslationX = canvasTranslationY = 8;
            openFileDialog1.InitialDirectory = saveFileDialog1.InitialDirectory = GetFolderPath(SpecialFolder.MyPictures) + '\\';
            openFileDialog1.Filter = vecFileDialogFilter;
            shapeContextMenuItems = new ShapeContextMenuItem[] {
                new ShapeContextMenuItem("Edit", EditShape),
                new ShapeContextMenuItem("Edit Properties", EditShapeData),
                new ShapeContextMenuDivider(),
                new ShapeContextMenuItem("Move Behind All", MoveBehindAll),
                new ShapeContextMenuItem("Move Closer", MoveCloser),
                new ShapeContextMenuItem("Move Further", MoveFurther),
                new ShapeContextMenuItem("Move In Front Of All", MoveInFrontOfAll),
                new ShapeContextMenuDivider(),
                new ShapeContextMenuItem("Replace With Lines", ReplaceWithLines),
                new ShapeContextMenuItem("Clear Style", ClearStyle),
                new ShapeContextMenuItem("Delete", DeleteShape)
            };
        }

        private void ReplaceWithLines(Shape _s)
        {
            foreach (Shape s in (editing.Contains(_s)) ? editing.ToArray() : new Shape[] { _s })
            {
                int id = shapes.IndexOf(s);
                if (id >= 0)
                {
                    if (s is Line) continue;
                    Line[] lines = s.ToLines();
                    shapes.RemoveAt(id);
                    shapes.InsertRange(id, lines);
                    id = editing.IndexOf(s);
                    if (id >= 0)
                    {
                        editing.RemoveAt(id);
                        editing.InsertRange(id, lines);
                        if (id < movingPointIds.Count)
                        {
                            movingPointIds.RemoveAt(id);
                            movingPointIds.InsertRange(id, Enumerable.Repeat(-1, lines.Length).ToArray());
                        }
                    }
                }
            }
        }
        private void MoveInFrontOfAll(Shape _s)
        {
            foreach (Shape s in (editing.Contains(_s)) ? editing : new List<Shape>(new Shape[] { _s }))
            {
                if (shapes.Contains(s))
                {
                    shapes.Remove(s);
                    shapes.Add(s);
                }
            }
        }
        private void MoveFurther(Shape _s)
        {
            foreach (Shape s in (editing.Contains(_s)) ? editing : new List<Shape>(new Shape[] { _s }))
            {
                int id = shapes.IndexOf(s);
                if (id > 0)
                {
                    shapes.RemoveAt(id);
                    shapes.Insert(id - 1, s);
                }
            }
        }
        private void MoveCloser(Shape _s)
        {
            foreach (Shape s in (editing.Contains(_s)) ? editing : new List<Shape>(new Shape[] { _s }))
            {
                int id = shapes.IndexOf(s);
                if (id >= 0 && id < shapes.Count - 1)
                {
                    shapes.RemoveAt(id);
                    shapes.Insert(id + 1, s);
                }
            }
        }
        private void MoveBehindAll(Shape _s)
        {
            foreach (Shape s in (editing.Contains(_s)) ? editing : new List<Shape>(new Shape[] { _s }))
            {
                if (shapes.Contains(s))
                {
                    shapes.Remove(s);
                    shapes.Insert(0, s);
                }
            }
        }
        private void EditShape(Shape s)
        {
            if (shapes.Contains(s) && !editing.Contains(s))
            {
                if (action != Actions.Edit) SelectActionEdit(this, EventArgs.Empty);
                editing.Add(s);
            }
        }
        private void EditShapeData(Shape _s)
        {
            foreach (Shape s in (editing.Contains(_s)) ? editing : new List<Shape>(new Shape[] { _s }))
            {
                if (shapes.Contains(s))
                {
                    ShapeEditor se;
                    if (shapeEditors.TryGetValue(s, out se))
                    {
                        se._Focus();
                        continue;
                    }
                    if (shapeEditors.Count >= 10)
                    {
                        MessageBox.Show("You have opened too many shape editors (max. count is 10).", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    if (s is Line)
                    {
                        shapeEditors.Add(s, se = new LineEditor((Line)s, this));
                        se._Show();
                        continue;
                    }
                    if (s is Rect)
                    {
                        shapeEditors.Add(s, se = new RectEditor((Rect)s, this));
                        se._Show();
                        continue;
                    }
                    if (s is Ellipse)
                    {
                        shapeEditors.Add(s, se = new EllipseEditor((Ellipse)s, this));
                        se._Show();
                        continue;
                    }
                }
            }
        }
        private void ClearStyle(Shape _s)
        {
            foreach (Shape s in (editing.Contains(_s)) ? editing : new List<Shape>(new Shape[] { _s }))
            {
                if (shapes.Contains(s))
                {
                    s.ClearStyle();
                    ShapeEditor se;
                    if (shapeEditors.TryGetValue(s, out se))
                    {
                        se._Refresh();
                    }
                }
            }
        }
        private void DeleteShape(Shape _s)
        {
            if (editing.Contains(_s))
            {
                string str = "Do you realy want to delete the selected shapes?\n";
                for (int i = 0; i < editing.Count && i < 10; i++)
                {
                    if (shapes.Contains(editing[i]))
                    {
                        str += "\n" + editing[i].ToString();
                    }
                }
                if (editing.Count > 10) str += "\n ... and " + (editing.Count - 10) + " more.";
                if (MessageBox.Show(str, "Deleting", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    foreach (Shape s in editing) shapes.Remove(s);
                    editing.Clear();
                    movingPointIds.Clear();
                }
            }
            else if (MessageBox.Show("Do you realy want to delete the selected shape?\n\n" + _s.ToString(), "Deleting", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                shapes.Remove(_s);
                int id = editing.IndexOf(_s);
                if (id >= 0)
                {
                    editing.RemoveAt(id);
                    if (id < movingPointIds.Count) movingPointIds.RemoveAt(id);
                }
                ShapeEditor se;
                if (shapeEditors.TryGetValue(_s, out se)) se._Close();
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            tool = Tools.Line;
            SelectImageLabel(pbLine, labelLine, groupShapes);
            action = Actions.Draw;
            SelectImageLabel(pbDraw, labelDraw, groupActions);
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            if (checkBoxGrid.Checked)
            {
                for (int i = 0; i < pictureBox1.Width + 16; i += 16)
                {
                    e.Graphics.DrawLine(gridPen, i + canvasTranslationX % 16, -1, i + canvasTranslationX % 16, pictureBox1.Height);
                }
                for (int i = 0; i < pictureBox1.Height + 16; i += 16)
                {
                    e.Graphics.DrawLine(gridPen, -1, i + canvasTranslationY % 16, pictureBox1.Width, i + canvasTranslationY % 16);
                }
            }
            e.Graphics.SmoothingMode = SmoothingMode.AntiAlias;
            foreach (Shape s in shapes)
            {
                if (editing.Contains(s))
                    s.RenderEditing(e.Graphics, canvasTranslation, editingAnimationTime);
                else s.Render(e.Graphics, canvasTranslation);
            }
            if (creatingShape)
            {
                int mx = mouseX, my = mouseY;
                AttachedPoint ap = Attach(mouseX, mouseY);
                if (ap != null)
                {
                    mx = ap.p.X;
                    my = ap.p.Y;
                    if (ap.i != null) e.Graphics.DrawImage(ap.i, mx - 5, my - 5);
                }
                switch (tool)
                {
                    case Tools.Line:
                        Line.PreRender(e.Graphics, mouseClickX, mouseClickY, mx, my);
                        break;
                    case Tools.Rect:
                        Rect.PreRender(e.Graphics, mouseClickX, mouseClickY, mx, my);
                        break;
                    case Tools.Ellipse:
                        Ellipse.PreRender(e.Graphics, mouseClickX, mouseClickY, mx, my);
                        break;
                }
            }
            else if (!mousePressed)
            {
                AttachedPoint ap = Attach(mouseX, mouseY, true);
                if (ap != null && ap.i != null)
                    e.Graphics.DrawImage(ap.i, ap.p.X - 5, ap.p.Y - 5);
            }
        }

        private AttachedPoint Attach(int mx, int my, bool ignoreChanging = false)
        {
            Point? p;
            if (pressedKeys.Contains(Keys.O) && (p = AttachToShape(mx, my, false, ignoreChanging)) != null)
                return new AttachedPoint(p.Value, Resource1.attaching);
            if (pressedKeys.Contains(Keys.I) && (p = AttachToIntersection(mx, my, ignoreChanging)) != null)
                return new AttachedPoint(p.Value, Resource1.attaching_i);
            if (pressedKeys.Contains(Keys.A) && (p = AttachToShape(mx, my, true, ignoreChanging)) != null)
                return new AttachedPoint(p.Value, Resource1.attaching_a);
            if (pressedKeys.Contains(Keys.ShiftKey))
                return new AttachedPoint(AttachToGrid(mx, my), Resource1.attaching_g);
            return null;
        }

        private Point AttachToGrid(int mx, int my)
        {
            return new Point((int)(Math.Floor((mx - (canvasTranslationX % 16) + 8) / 16.0) * 16) + canvasTranslationX % 16,
                (int)(Math.Floor((my - (canvasTranslationY % 16) + 8) / 16.0) * 16) + canvasTranslationY % 16);
        }

        private Point? AttachToIntersection(int mx, int my, bool ignoreChanging = false)
        {
            List<Point> ps = new List<Point>();
            int id;
            for (int i = 0; i < shapes.Count - 1; i++)
            {
                id = editing.IndexOf(shapes[i]);
                if (!ignoreChanging || id < 0 || MovingPointId(id) < 0)
                {
                    for (int j = i + 1; j < shapes.Count; j++)
                    {
                        id = editing.IndexOf(shapes[j]);
                        if (!ignoreChanging || id < 0 || MovingPointId(id) < 0)
                            shapes[i].AttachPointToIntersection(mx, my, shapes[j], canvasTranslation, ps);
                    }
                }
            }
            if (ps.Count == 0)
                return null;
            if (ps.Count == 1)
                return ps[0];
            long mindist = -1, l;
            Point r = ps[0], p;
            for (int i = 0; i < ps.Count; i++)
            {
                p = ps[0];
                l = p.X * p.X + p.Y * p.Y;
                if (l > mindist)
                {
                    r = p;
                    mindist = l;
                }
            }
            return r;
        }

        private int MovingPointId(int id)
        {
            if (id < 0 || id >= movingPointIds.Count) return -1;
            return movingPointIds[id];
        }

        private Point? AttachToShape(int mx, int my, bool sec = false, bool ignoreChanging = false)
        {
            List<Point> ps = new List<Point>();
            int id;
            foreach (Shape s in shapes)
            {
                id = editing.IndexOf(s);
                if (!ignoreChanging || id < 0 || MovingPointId(id) < 0)
                {
                    if (sec)
                        s.AttachPointS(mx, my, canvasTranslation, ps);
                    else
                        s.AttachPointP(mx, my, canvasTranslation, ps);
                }
            }
            if (ps.Count == 0)
                return null;
            if (ps.Count == 1)
                return ps[0];
            long mindist = -1, l;
            Point r = ps[0], p;
            for (int i = 0; i < ps.Count; i++)
            {
                p = ps[0];
                l = p.X * p.X + p.Y * p.Y;
                if (l > mindist)
                {
                    r = p;
                    mindist = l;
                }
            }
            return r;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            pictureBox1.Refresh();
            editingAnimationTime = (++editingAnimationTime) % 80;
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void SelectImageLabel(PictureBox p, Label l, GroupBox gb)
        {
            foreach (Control c in gb.Controls)
            {
                if (c is PictureBox)
                {
                    c.Text = (c == p) ? "e" : "d";
                    c.Refresh();
                }
                if (c is Label)
                    c.Font = new Font(c.Font, (c == l) ? FontStyle.Bold : FontStyle.Regular);
            }
        }

        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                mousePressed = true;
                switch (action)
                {
                    case Actions.Draw:
                        mouseClickX = e.X;
                        mouseClickY = e.Y;
                        AttachedPoint ap = Attach(e.X, e.Y);
                        if (ap != null)
                        {
                            mouseClickX = ap.p.X;
                            mouseClickY = ap.p.Y;
                        }
                        break;
                    case Actions.Edit:
                        if (!pressedKeys.Contains(Keys.ControlKey)) editing.Clear();
                        for (int i = shapes.Count - 1; i >= 0; i--)
                        {
                            if (shapes[i].Hit(e.X, e.Y, canvasTranslation))
                            {
                                editing.Add(shapes[i]);
                                if (!pressedKeys.Contains(Keys.ControlKey)) break;
                            }
                        }
                        break;
                    case Actions.Move:
                        for (int i = shapes.Count - 1; i >= 0; i--)
                        {
                            if (shapes[i].Hit(e.X, e.Y, canvasTranslation))
                            {
                                movingShape = shapes[i];
                                break;
                            }
                        }
                        break;
                }
            }
            else if (e.Button == MouseButtons.Middle)
            {
                mouseMPressed = true;
                movingPointIds.Clear();
                int mx = mouseX - canvasTranslationX, my = mouseY - canvasTranslationY;
                for (int i = 0; i < editing.Count; i++)
                {
                    Point[] ps = editing[i].EditingPoints();
                    movingPointIds.Add(-1);
                    for (int j = 0; j < ps.Length; j++)
                    {
                        if (Util.DistSqr(mx, my, ps[j].X, ps[j].Y) < Shape.editingPointMaxDistSqr)
                        {
                            movingPointIds[i] = j;
                            break;
                        }
                    }
                    if (movingPointIds[i] >= 0 && !pressedKeys.Contains(Keys.X)) break;
                }
            }
        }
        private void pictureBox1_MouseLeave(object sender, EventArgs e)
        {
            mousePressed = false;
            mouseMPressed = false;
        }
        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            if (action == Actions.MoveCanvas && movingCanvas)
            {
                canvasTranslationX += e.X - mouseX;
                canvasTranslationY += e.Y - mouseY;
            }
            else if (action == Actions.Move && movingShape != null)
            {
                movingShape.MoveAll(e.X - mouseX, e.Y - mouseY);
            }
            else if (action == Actions.Edit && mouseMPressed)
            {
                int mx, my;
                for (int i = 0; i < editing.Count; i++)
                {
                    int j = MovingPointId(i);
                    if (j >= 0)
                    {
                        Shape s = editing[i];
                        Point[] ps = s.EditingPoints();
                        AttachedPoint ap = Attach(mouseX, mouseY, true);
                        mx = ((ap != null) ? ap.p.X : e.X) - canvasTranslationX;
                        my = ((ap != null) ? ap.p.Y : e.Y) - canvasTranslationY;
                        Tuple<bool, int> t = editing[i].MovePoint(mx - ps[j].X, my - ps[j].Y, j);
                        movingPointIds[i] = t.Item2;
                    }
                }
            }
            mouseX = e.X;
            mouseY = e.Y;
        }
        private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                if (creatingShape && action == Actions.Draw)
                {
                    int mx = mouseX, my = mouseY;
                    AttachedPoint ap = Attach(mouseX, mouseY);
                    if (ap != null)
                    {
                        mx = ap.p.X;
                        my = ap.p.Y;
                    }
                    switch (tool)
                    {
                        case Tools.Line:
                            if (mouseClickX != mx || mouseClickY != my)
                            {
                                shapes.Add(new Line(mouseClickX - canvasTranslationX, mouseClickY - canvasTranslationY,
                                    mx - canvasTranslationX, my - canvasTranslationY, hslColorPicker.Color));
                                saved = false;
                            }
                            break;
                        case Tools.Rect:
                            if (mouseClickX != mx && mouseClickY != my)
                            {
                                shapes.Add(new Rect(mouseClickX - canvasTranslationX, mouseClickY - canvasTranslationY,
                                    mx - canvasTranslationX, my - canvasTranslationY, hslColorPicker.Color));
                                saved = false;
                            }
                            break;
                        case Tools.Ellipse:
                            if (mouseClickX != mx && mouseClickY != my)
                            {
                                shapes.Add(new Ellipse(mouseClickX - canvasTranslationX, mouseClickY - canvasTranslationY,
                                    mx - canvasTranslationX, my - canvasTranslationY, hslColorPicker.Color));
                                saved = false;
                            }
                            break;
                    }
                }
                mousePressed = false;
                movingShape = null;
            }
            else if (e.Button == MouseButtons.Middle)
            {
                if (action == Actions.Edit)
                {
                    int mx, my;
                    for (int i = 0; i < editing.Count; i++)
                    {
                        int j = MovingPointId(i);
                        if (j >= 0)
                        {
                            Shape s = editing[i];
                            Point[] ps = s.EditingPoints();
                            AttachedPoint ap = Attach(mouseX, mouseY, true);
                            mx = ((ap != null) ? ap.p.X : e.X) - canvasTranslationX;
                            my = ((ap != null) ? ap.p.Y : e.Y) - canvasTranslationY;
                            Tuple<bool, int> t = editing[i].MovePoint(mx - ps[j].X, my - ps[j].Y, j);
                            if (!t.Item1)
                            {
                                shapes.Remove(s);
                                editing.RemoveAt(i);
                                movingPointIds.RemoveAt(i);
                                i--;
                            }
                            else movingPointIds[i] = t.Item2;
                        }
                    }
                }
                mouseMPressed = false;
            }
        }

        private void buttonColor_Click(object sender, EventArgs e)
        {
            hslColorPicker.EnableAlpha = false;
            Color c = hslColorPicker.Color;
            if (hslColorPicker.ShowDialog() == DialogResult.OK)
                pbColor.Refresh();
            else hslColorPicker.Color = c;
        }

        private void pbColor_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.Clear(hslColorPicker.Color);
            e.Graphics.DrawRectangle(new Pen(Color.FromArgb(128, 255, 255, 255)), 0, 0, 24, 24);
            e.Graphics.DrawRectangle(new Pen(Color.FromArgb(128, 0, 0, 0)), -1, -1, 24, 24);
        }

        private void pbGrid_Paint(object sender, PaintEventArgs e)
        {
            if (checkBoxGrid.Checked)
                e.Graphics.DrawImageUnscaled(Resource1.icon_grid, 0, 0);
            else
                e.Graphics.DrawImage(Resource1.icon_grid, new System.Drawing.Rectangle(0, 0, 24, 24), 0, 0, 24, 24, GraphicsUnit.Pixel, grayscale);
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveFileDialog1.Filter = vecFileDialogFilter;
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    if (saveFileDialog1.FileName.EndsWith(".svg"))
                    {
                        using (StreamWriter sw = new StreamWriter(saveFileDialog1.FileName))
                        {
                            sw.WriteLine("<?xml version=\"1.0\"?>");
                            sw.WriteLine("<!DOCTYPE svg public \"-//W3C//DTD SVG 1.1//EN\" \"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\">");
                            Rectangle cs = CanvasSize;
                            sw.WriteLine(string.Format("<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"{0}\" height=\"{1}\" > ", cs.Width, cs.Height));
                            string tab = "    ";
                            foreach (Shape s in shapes)
                            {
                                sw.Write(tab);
                                sw.WriteLine(s.RenderSVG(new Point(-cs.X, -cs.Y)));
                            }
                            sw.WriteLine("</svg>");
                            sw.Flush();
                        }
                        saved = true;
                        fileName = saveFileDialog1.FileName.Substring(saveFileDialog1.FileName.LastIndexOf('\\') + 1);
                    }
                    else if (saveFileDialog1.FileName.EndsWith(".vbd"))
                    {
                        using (BinaryWriter bw = new BinaryWriter(new FileStream(saveFileDialog1.FileName, FileMode.Create)))
                        {
                            foreach (Shape s in shapes)
                            {
                                s.SaveBinary(bw);
                            }
                            bw.Flush();
                        }
                        saved = true;
                        fileName = saveFileDialog1.FileName.Substring(saveFileDialog1.FileName.LastIndexOf('\\') + 1);
                    }
                }
                catch (Exception ex)
                {
#if DEBUG
                    MessageBox.Show(ex.Message);
#else
                    MessageBox.Show("Error has appeared during exporting.");
#endif
                }
            }
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!saved)
            {
                DialogResult dr = MessageBox.Show("You have some unsaved changes.\nDo you want to save them now?", "Question", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                if (dr == DialogResult.Cancel)
                    return;
                if (dr == DialogResult.Yes)
                    saveToolStripMenuItem_Click(sender, e);
            }
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    shapeEditors.Clear();
                    if (openFileDialog1.FileName.EndsWith(".svg"))
                    {
                        XmlReaderSettings settings = new XmlReaderSettings();
                        settings.DtdProcessing = DtdProcessing.Ignore;
                        settings.ValidationType = ValidationType.None;
                        using (XmlReader xr = XmlReader.Create(openFileDialog1.FileName, settings))
                        {
                            SVGUtil.ReadResult rr = SVGUtil.Read(xr);
                            if (rr.errors.Count > 0)
                            {
                                string s = "Some problems have appeared during loading:";
                                foreach (string err in rr.errors)
                                    s += '\n' + err;
                                MessageBox.Show(s, "Problems", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                            else
                            {
                                shapes.Clear();
                                shapes.AddRange(rr.shapes);
                                fileName = openFileDialog1.SafeFileName;
                                saved = true;
                                if (rr.unrecognizedElements.Count > 0)
                                {
                                    string s = "The file was not fully loaded, following elements were not recognized:\n";
                                    bool first = true;
                                    foreach (string ue in rr.unrecognizedElements)
                                    {
                                        if (!first)
                                            s += ", " + ue;
                                        else
                                        {
                                            s += ue;
                                            first = false;
                                        }
                                    }
                                    MessageBox.Show(s, "Problems", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    saved = false;
                                }
                            }
                        }
                    }
                    else if (openFileDialog1.FileName.EndsWith(".vbd"))
                    {
                        using (BinaryReader br = new BinaryReader(new FileStream(saveFileDialog1.FileName, FileMode.Open)))
                        {
                            shapes.Clear();
                            while (br.BaseStream.Position < br.BaseStream.Length)
                            {
                                Type t = Shape.ShapeFromId(br.ReadByte());
                                if (t != null)
                                {
                                    shapes.Add((Shape)t.GetConstructor(new Type[] { br.GetType() }).Invoke(new object[] { br }));
                                }
                            }
                        }
                        fileName = openFileDialog1.SafeFileName;
                        saved = true;
                    }
                }
                catch (Exception ex)
                {
#if DEBUG
                    MessageBox.Show(ex.Message);
#else
                    MessageBox.Show("Error has appeared during loading the file.");
#endif
                }
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!saved)
            {
                DialogResult dr = MessageBox.Show("You have some unsaved changes.\nDo you want to save them now?", "Question", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                if (dr == DialogResult.Cancel)
                    e.Cancel = true;
                if (dr == DialogResult.Yes)
                    saveToolStripMenuItem_Click(sender, e);
            }
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!saved)
            {
                DialogResult dr = MessageBox.Show("You have some unsaved changes.\nDo you want to save them now?", "Question", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                if (dr == DialogResult.Cancel)
                    return;
                if (dr == DialogResult.Yes)
                    saveToolStripMenuItem_Click(sender, e);
            }
            shapes.Clear();
            shapeEditors.Clear();
            fileName = null;
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            pressedKeys.Add(e.KeyCode);
            if (e.KeyCode == Keys.Delete && action == Actions.Edit && editing != null)
            {
                if (editing.Count > 0) DeleteShape(editing[0]);
            }
        }

        private void Form1_KeyUp(object sender, KeyEventArgs e)
        {
            pressedKeys.Remove(e.KeyCode);
            /*string s = "Keys:";
            foreach (Keys k in pressedKeys)
            {
                s += '\n' + k.ToString();
            }
            label1.Text = s;*/
        }

        private void exportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveFileDialog1.Filter = imgFileDialogFilter;
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    Rectangle cs = CanvasSize;
                    Bitmap bmp = new Bitmap(cs.Width, cs.Height);
                    Graphics g = Graphics.FromImage(bmp);
                    g.Clear(saveFileDialog1.FileName.EndsWith(".png") ? Color.Transparent : Color.White);
                    g.SmoothingMode = SmoothingMode.HighQuality;
                    foreach (Shape s in shapes)
                    {
                        s.Render(g, new Point(-cs.X, -cs.Y));
                    }
                    bmp.Save(saveFileDialog1.FileName);
                }
                catch (Exception ex)
                {
#if DEBUG
                    MessageBox.Show(ex.Message);
#else
                    MessageBox.Show("Error has appeared during exporting.");
#endif
                }
            }
        }

        private void checkBoxGrid_CheckedChanged(object sender, EventArgs e)
        {
            pbGrid.Refresh();
        }

        private void pb_Paint(object sender, PaintEventArgs e)
        {
            Image i = null;
            if (sender == pbLine) i = Resource1.icon_line;
            else if (sender == pbRect) i = Resource1.icon_rect;
            else if (sender == pbCirc) i = Resource1.icon_ellipse;
            else if (sender == pbDraw) i = Resource1.icon_draw;
            else if (sender == pbEdit) i = Resource1.icon_edit;
            else if (sender == pbMove) i = Resource1.icon_move;
            else if (sender == pbMovC) i = Resource1.icon_move_canvas;
            if (i != null)
            {
                if (((Control)sender).Text == "e")
                    e.Graphics.DrawImageUnscaled(i, 0, 0);
                else
                    e.Graphics.DrawImage(i, new Rectangle(0, 0, 24, 24), 0, 0, 24, 24, GraphicsUnit.Pixel, grayscale);
            }
        }

        private void pictureBox1_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                foreach (Shape s in shapes)
                {
                    if (s.Hit(e.X, e.Y, canvasTranslation))
                    {
                        ShapeContextMenu scm = new ShapeContextMenu(s, shapeContextMenuItems);
                        scm.Show(pictureBox1, e.X, e.Y);
                        break;
                    }
                }
            }
        }

        private void selectAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SelectActionEdit(this, EventArgs.Empty);
            editing.Clear();
            movingPointIds.Clear();
            editing.AddRange(shapes);
        }

        private void deselectAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            editing.Clear();
            movingPointIds.Clear();
        }

        private void resetCanvasPositionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            canvasTranslationX = canvasTranslationY = 8;
        }

        private void SelectToolLine(object sender, EventArgs e)
        {
            tool = Tools.Line;
            SelectImageLabel(pbLine, labelLine, groupShapes);
        }

        private void turtleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TurtleScriptInput tsi = new TurtleScriptInput(this);
            tsi.ShowDialog();
        }

        private void SelectToolRectangle(object sender, EventArgs e)
        {
            tool = Tools.Rect;
            SelectImageLabel(pbRect, labelRect, groupShapes);
        }
        private void SelectToolEllipse(object sender, EventArgs e)
        {
            tool = Tools.Ellipse;
            SelectImageLabel(pbCirc, labelCirc, groupShapes);
        }

        private void SelectActionDraw(object sender, EventArgs e)
        {
            action = Actions.Draw;
            SelectImageLabel(pbDraw, labelDraw, groupActions);
            editing.Clear();
            movingShape = null;
            groupShapes.Visible = true;
        }
        private void SelectActionEdit(object sender, EventArgs e)
        {
            action = Actions.Edit;
            SelectImageLabel(pbEdit, labelEdit, groupActions);
            editing.Clear();
            movingShape = null;
            groupShapes.Visible = false;
        }
        private void SelectActionMove(object sender, EventArgs e)
        {
            action = Actions.Move;
            SelectImageLabel(pbMove, labelMove, groupActions);
            editing.Clear();
            movingShape = null;
            groupShapes.Visible = false;
        }
        private void SelectActionMoveCanvas(object sender, EventArgs e)
        {
            action = Actions.MoveCanvas;
            SelectImageLabel(pbMovC, labelMovC, groupActions);
            editing.Clear();
            movingShape = null;
            groupShapes.Visible = false;
        }
    }
}
