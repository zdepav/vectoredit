﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("VectorEditTests")]
namespace VectorEdit
{
    internal static class Util
    {
        public static BinaryWriter WriteR(this BinaryWriter bw, int value) { bw.Write(value); return bw; }
        public static BinaryWriter WriteR(this BinaryWriter bw, bool value) { bw.Write(value); return bw; }
        public static BinaryWriter WriteR(this BinaryWriter bw, short value) { bw.Write(value); return bw; }
        public static BinaryWriter WriteR(this BinaryWriter bw, byte value) { bw.Write(value); return bw; }
        public static BinaryWriter WriteR(this BinaryWriter bw, string value) { bw.Write(value); return bw; }
        public static BinaryWriter WriteR(this BinaryWriter bw, long value) { bw.Write(value); return bw; }
        public static BinaryWriter WriteR(this BinaryWriter bw, float value) { bw.Write(value); return bw; }
        public static BinaryWriter WriteR(this BinaryWriter bw, double value) { bw.Write(value); return bw; }
        public static BinaryWriter WriteR(this BinaryWriter bw, uint value) { bw.Write(value); return bw; }
        public static BinaryWriter WriteR(this BinaryWriter bw, ulong value) { bw.Write(value); return bw; }
        public static BinaryWriter WriteR(this BinaryWriter bw, ushort value) { bw.Write(value); return bw; }
        public static BinaryWriter WriteR(this BinaryWriter bw, char value) { bw.Write(value); return bw; }

        /// <summary>
        /// HSLA to RGBA converting method
        /// </summary>
        /// <param name="hue">Hue (0 - 360°)</param>
        /// <param name="saturation">Saturation (0 - 100%)</param>
        /// <param name="lightness">Lightness (0 - 100%)</param>
        /// <param name="alpha">Alpha (0 - 1)</param>
        /// <exception cref="System.ArgumentOutOfRangeException">Thrown when some parameter is out of range.</exception>
        /// <returns>RGBA color</returns>
        public static Color RGBAFromHSLA(float hue, float saturation, float lightness, float alpha)
        {
            if (!Cmp(Clamp(hue, 0, 360), hue))
                throw new ArgumentOutOfRangeException("hue", hue, "Hue must be between 0 and 360°!");
            hue /= 60;
            saturation /= 100;
            lightness /= 100;

            float c = (1 - Math.Abs(2 * lightness - 1)) * saturation;
            float x = c * (1 - Math.Abs((hue) % 2 - 1));
            float m = lightness - c / 2;
            float r, g, b;
            if (hue < 1)
            { r = c; g = x; b = 0; }
            else if (hue < 2)
            { r = x; g = c; b = 0; }
            else if (hue < 3)
            { r = 0; g = c; b = x; }
            else if (hue < 4)
            { r = 0; g = x; b = c; }
            else if (hue < 5)
            { r = x; g = 0; b = c; }
            else { r = c; g = 0; b = x; }
            return Color.FromArgb((byte)(alpha * 255), (byte)((r + m) * 255), (byte)((g + m) * 255), (byte)((b + m) * 255));
        }

        /// <summary>
        /// HSL to RGBA converting method
        /// </summary>
        /// <param name="h">Hue (0 - 360°)</param>
        /// <param name="s">Saturation (0 - 100%)</param>
        /// <param name="l">Lightness (0 - 100%)</param>
        /// <exception cref="System.ArgumentOutOfRangeException">Thrown when some parameter is out of range.</exception>
        /// <returns>RGBA color</returns>
        public static Color RGBAFromHSL(float h, float s, float l)
        {
            return RGBAFromHSLA(h, s, l, 1);
        }

        /// <summary>
        /// for light colors returns <see cref="Color" />.White, for dark colors returns <see cref="Color" />.Black
        /// </summary>
        /// <param name="c"></param>
        /// <returns><see cref="Color" />.Black or <see cref="Color" />.White</returns>
        public static Color WBFromRGBA(Color c)
        {
            return (0.3f * c.R + 0.59f * c.G + 0.11f * c.B) > 127.5f ? Color.White : Color.Black;
        }

        /// <summary>
        /// for light colors returns <see cref="Color" />.Black, for dark colors returns <see cref="Color" />.White
        /// </summary>
        /// <param name="c"></param>
        /// <returns><see cref="Color" />.Black or <see cref="Color" />.White</returns>
        public static Color InvWBFromRGBA(Color c)
        {
            return (0.3f * c.R + 0.59f * c.G + 0.11f * c.B) > 127.5f ? Color.Black : Color.White;
        }

        private static NumberStyles numberStyle = NumberStyles.AllowDecimalPoint;
        private static CultureInfo numberCulture = CultureInfo.CreateSpecificCulture("en-us");

        /// <summary>
        /// TryParse method for float with en-us localization
        /// </summary>
        /// <param name="s">string to parse</param>
        /// <param name="value">parsed value or 0 if not successful</param>
        /// <returns>true if parsing was successful, false if not</returns>
        public static bool TryParse(string s, out float value)
        {
            return float.TryParse(s, numberStyle, numberCulture, out value);
        }

        /// <summary>
        /// compares two floating point numbers with tolerated difference of double.Epsilon * 1000
        /// </summary>
        /// <param name="d1">first number to compare</param>
        /// <param name="d2">second number to compare</param>
        /// <returns>true if the difference between d1 and d2 is smaller than double.Epsilon * 1000, false if otherwise</returns>
        public static bool Cmp(double d1, double d2)
        {
            return Math.Abs(d1 - d2) < (double.Epsilon * 1000);
        }

        /// <summary>
        /// compares two floating point numbers with tolerated difference of float.Epsilon * 1000
        /// </summary>
        /// <param name="f1">first number to compare</param>
        /// <param name="f2">second number to compare</param>
        /// <returns>true if the difference between f1 and f2 is smaller than float.Epsilon * 1000, false if otherwise</returns>
        public static bool Cmp(float f1, float f2)
        {
            return Math.Abs(f1 - f2) < (float.Epsilon * 1000);
        }

        public static int Clamp(int value, int min, int max)
        {
            return (value > max) ? max : ((value < min) ? min : value);
        }

        public static float Clamp(float value, float min, float max)
        {
            return (value > max) ? max : ((value < min) ? min : value);
        }

        public static double Clamp(double value, double min, double max)
        {
            return (value > max) ? max : ((value < min) ? min : value);
        }

        /// <summary>
        /// returns distance between points [x1, x2] and [y1, y2]
        /// </summary>
        /// <param name="x1">x coord of 1st point</param>
        /// <param name="y1">y coord of 1st point</param>
        /// <param name="x2">x coord of 2nd point</param>
        /// <param name="y2">y coord of 2nd point</param>
        /// <returns>distance between points [x1, x2] and [y1, y2]</returns>
        public static double Dist(double x1, double y1, double x2, double y2)
        {
            return Math.Sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
        }

        /// <summary>
        /// returns squared distance between points [x1, x2] and [y1, y2]
        /// </summary>
        /// <param name="x1">x coord of 1st point</param>
        /// <param name="y1">y coord of 1st point</param>
        /// <param name="x2">x coord of 2nd point</param>
        /// <param name="y2">y coord of 2nd point</param>
        /// <returns>squared distance between points [x1, x2] and [y1, y2]</returns>
        public static double DistSqr(double x1, double y1, double x2, double y2)
        {
            return (x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2);
        }

        /// <summary>
        /// returns true if i is between a and b and i != a and i != b
        /// </summary>
        /// <param name="i">number to test</param>
        /// <param name="a">lower boundary</param>
        /// <param name="b">higher boundary</param>
        /// <exception cref="System.ArgumentException">Thrown if a >= b.</exception>
        /// <returns>true if i is between a and b and i != a and i != b</returns>
        public static bool IsBetween(int i, int a, int b)
        {
            if (a >= b) throw new ArgumentException();
            return i > a && i < b;
        }

        internal static string replaceCommands(string code)
        {
            string s2 = "";
            int l = code.Length;
            char c;
            for (int i = 0; i < l; i++)
            {
                c = code[i];
                switch (c)
                {
                    case '=': s2 += 'M'; break;
                    case '|': s2 += 'F'; break;
                    case '^': s2 += 'B'; break;
                    case '<': s2 += 'L'; break;
                    case '>': s2 += 'R'; break;
                    //case '#': s2 += 'C'; break;
                    case '[': s2 += 'U'; break;
                    case ']': s2 += 'O'; break;
                    case 'S': s2 += '{'; break;
                    case 'E': s2 += '}'; break;
                    case '~': s2 += 'Y'; break;
                    case 'α': s2 += 'a'; break;
                    case 'π': s2 += 'p'; break;
                    case '×': s2 += '*'; break;
                    case '÷': s2 += '/'; break;
                    case '✓': s2 += 't'; break;
                    case '✗': s2 += 'f'; break;
                    default: s2 += c; break;
                }
            }
            return s2;
        }

    }
}
