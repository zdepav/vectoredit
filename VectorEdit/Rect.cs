﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VectorEdit
{
    /// <summary>
    /// Rectangle shape class
    /// </summary>
    internal class Rect : Shape
    {
        public override int X { get { return x; } }
        public override int Y { get { return y; } }
        public override int X2 { get { return x + w; } }
        public override int Y2 { get { return y + h; } }
        public int x, y;
        public int w { get; set; }
        public int h { get; set; }

        public Color lineColor, fillColor;

        public byte lineWidth;
        byte lineWidth_d2 { get { return (byte)(lineWidth / 2); } }

        public Rect(int x1, int y1, int x2, int y2, Color color, byte lineWidth = 2)
        {
            x = Math.Min(x1, x2);
            w = Math.Max(x1, x2) - x;
            y = Math.Min(y1, y2);
            h = Math.Max(y1, y2) - y;
            lineColor = color;
            fillColor = Color.Transparent;
            this.lineWidth = lineWidth;
        }

        public Rect(BinaryReader br)
        {
            x = br.ReadInt32();
            y = br.ReadInt32();
            w = br.ReadInt32();
            h = br.ReadInt32();
            fillColor = Color.FromArgb(br.ReadByte(), br.ReadByte(), br.ReadByte(), br.ReadByte());
            lineColor = Color.FromArgb(br.ReadByte(), br.ReadByte(), br.ReadByte());
            lineWidth = br.ReadByte();
        }

        public override void Render(Graphics g, Point canvasTranslation)
        {
            g.FillRectangle(new SolidBrush(fillColor),
                x + canvasTranslation.X, y + canvasTranslation.Y, w, h);
            g.DrawRectangle(new Pen(lineColor, lineWidth),
                x + canvasTranslation.X, y + canvasTranslation.Y, w, h);
        }

        public override void RenderEditing(Graphics g, Point canvasTranslation, int time)
        {
            int _x = X + canvasTranslation.X, _y = Y + canvasTranslation.Y;
            int _x2 = X2 + canvasTranslation.X, _y2 = Y2 + canvasTranslation.Y;
            int a = (int)Math.Round(255 * ((1 + Math.Cos(Math.PI * time / 40.0)) / 2));
            if (a > 0)
            {
                g.FillRectangle(new SolidBrush(Color.FromArgb((int)(a * (fillColor.A / 255.0)), fillColor.R, fillColor.G, fillColor.B)), _x, _y, w, h);
                g.DrawRectangle(new Pen(Color.FromArgb(a, lineColor.R, lineColor.G, lineColor.B), lineWidth), _x, _y, w, h);
            }
            g.DrawRectangle(new Pen(new HatchBrush(HatchStyle.LargeCheckerBoard, Color.FromArgb(255 - a, 0, 0, 255), Color.Transparent)), _x, _y, w, h);
            RenderEditingPoint(g, new Point(_x, _y));
            RenderEditingPoint(g, new Point(_x2, _y2));
            RenderEditingPoint(g, new Point(_x2, _y));
            RenderEditingPoint(g, new Point(_x, _y2));
        }

        public override Point[] EditingPoints()
        {
            return new Point[] { new Point(X, Y), new Point(X2, Y), new Point(X2, Y2), new Point(X, Y2) };
        }

        public override string RenderSVG(Point canvasTranslation)
        {
            return string.Format("<rect x=\"{0}\" y=\"{1}\" width=\"{2}\" height=\"{3}\" style=\"fill: #{4:x2}{5:x2}{6:x2}; stroke: #{7:x2}{8:x2}{9:x2}; stroke-width: {10}px; fill-opacity: {11};\" />",
                x + canvasTranslation.X, y + canvasTranslation.Y, w, h, fillColor.R, fillColor.G, fillColor.B, lineColor.R, lineColor.G, lineColor.B, lineWidth, fillColor.A / 255.0);
        }

        public override void SaveBinary(BinaryWriter bw)
        {
            bw.Write(ShapeToId(GetType()));
            bw.WriteR(x).WriteR(y).WriteR(w).Write(h);
            bw.WriteR(fillColor.A).WriteR(fillColor.R).WriteR(fillColor.G).Write(fillColor.B);
            bw.WriteR(lineColor.R).WriteR(lineColor.G).Write(lineColor.B);
            bw.Write(lineWidth);
        }

        public static void PreRender(Graphics g, int x1, int y1, int x2, int y2)
        {
            int _x = Math.Min(x1, x2), _y = Math.Min(y1, y2);
            g.DrawRectangle(preRenderPen, _x, _y, Math.Max(x1, x2) - _x, Math.Max(y1, y2) - _y);
        }

        public override bool Hit(int mx, int my, Point canvasTranslation)
        {
            int lwd2 = lineWidth_d2 + 2;
            mx -= canvasTranslation.X;
            my -= canvasTranslation.Y;
            return ((((mx > x - lwd2) && (mx < x + lwd2)) || ((mx > X2 - lwd2) && (mx < X2 + lwd2))) && ((my > y - lwd2) && (my < Y2 + lwd2))) ||
                   ((((my > y - lwd2) && (my < y + lwd2)) || ((my > Y2 - lwd2) && (my < Y2 + lwd2))) && ((mx > x - lwd2) && (mx < X2 + lwd2)));
        }

        public override void AttachPointP(int mx, int my, Point canvasTranslation, List<Point> points)
        {
            mx -= canvasTranslation.X;
            my -= canvasTranslation.Y;
            int x2 = X2, y2 = Y2, xd = x - mx, x2d = x2 - mx, yd = y - my, y2d = y2 - my;
            xd *= xd; x2d *= x2d; yd *= yd; y2d *= y2d;
            if ((xd + yd) < attachmentEpsylonSqr)
                points.Add(new Point(x + canvasTranslation.X, y + canvasTranslation.Y));
            if ((xd + y2d) < attachmentEpsylonSqr)
                points.Add(new Point(x + canvasTranslation.X, y2 + canvasTranslation.Y));
            if ((x2d + yd) < attachmentEpsylonSqr)
                points.Add(new Point(x2 + canvasTranslation.X, y + canvasTranslation.Y));
            if ((x2d + y2d) < attachmentEpsylonSqr)
                points.Add(new Point(x2 + canvasTranslation.X, y2 + canvasTranslation.Y));
        }

        public override void AttachPointS(int mx, int my, Point canvasTranslation, List<Point> points)
        {
            mx -= canvasTranslation.X;
            my -= canvasTranslation.Y;
            int x2 = X2, y2 = Y2;
            if (mx > x && mx < x2)
            {
                if (Math.Abs(my - y) < attachmentEpsylon)
                    points.Add(new Point(mx + canvasTranslation.X, y + canvasTranslation.Y));
                if (Math.Abs(my - y2) < attachmentEpsylon)
                    points.Add(new Point(mx + canvasTranslation.X, y2 + canvasTranslation.Y));
            }
            if (my > y && my < y2)
            {
                if (Math.Abs(mx - x) < attachmentEpsylon)
                    points.Add(new Point(x + canvasTranslation.X, my + canvasTranslation.Y));
                if (Math.Abs(mx - x2) < attachmentEpsylon)
                    points.Add(new Point(x2 + canvasTranslation.X, my + canvasTranslation.Y));
            }
        }

        public override void AttachPointToIntersection(int mx, int my, Shape s, Point canvasTranslation, List<Point> points)
        {
            if (s is Ellipse)
            {
                Ellipse e = (Ellipse)s;
                PointF[] pts = e.ToPolyline(72);
                PointF A = new PointF(x, Y2), B = new PointF(X2, Y2), C = new PointF(X2, y), D = new PointF(x, y);
                int i2;
                Point p;
                List<Point> ps = new List<Point>();
                for (int i = 0; i < 72; i++)
                {
                    i2 = (i + 1) % 72;
                    Intersections(A, B, C, D, pts[i], pts[i2], ps);
                }
                mx -= canvasTranslation.X;
                my -= canvasTranslation.Y;
                foreach (Point pt in ps)
                {
                    p = e.MoveToEdge(pt.X, pt.Y);
                    if (Util.DistSqr(p.X, p.Y, mx, my) < attachmentEpsylonSqr)
                        points.Add(new Point(p.X + canvasTranslation.X, p.Y + canvasTranslation.Y));
                }
            }
            else if (s is Rect)
            {
                mx -= canvasTranslation.X;
                my -= canvasTranslation.Y;
                int x2 = X2, y2 = Y2, sX2 = s.X2, sY2 = s.Y2;
                if (Util.IsBetween(x, s.X, sX2))
                {
                    if (Util.IsBetween(s.Y, y, y2) && Util.DistSqr(x, s.Y, mx, my) < attachmentEpsylonSqr)
                        points.Add(new Point(x + canvasTranslation.X, s.Y + canvasTranslation.Y));
                    if (Util.IsBetween(sY2, y, y2) && Util.DistSqr(x, sY2, mx, my) < attachmentEpsylonSqr)
                        points.Add(new Point(x + canvasTranslation.X, sY2 + canvasTranslation.Y));
                }
                if (Util.IsBetween(x2, s.X, sX2))
                {
                    if (Util.IsBetween(s.Y, y, y2) && Util.DistSqr(x2, s.Y, mx, my) < attachmentEpsylonSqr)
                        points.Add(new Point(x2 + canvasTranslation.X, s.Y + canvasTranslation.Y));
                    if (Util.IsBetween(sY2, y, y2) && Util.DistSqr(x2, sY2, mx, my) < attachmentEpsylonSqr)
                        points.Add(new Point(x2 + canvasTranslation.X, sY2 + canvasTranslation.Y));
                }
                if (Util.IsBetween(y, s.Y, sY2))
                {
                    if (Util.IsBetween(s.X, x, x2) && Util.DistSqr(s.X, y, mx, my) < attachmentEpsylonSqr)
                        points.Add(new Point(s.X + canvasTranslation.X, y + canvasTranslation.Y));
                    if (Util.IsBetween(sX2, x, x2) && Util.DistSqr(sX2, y, mx, my) < attachmentEpsylonSqr)
                        points.Add(new Point(sX2 + canvasTranslation.X, y + canvasTranslation.Y));
                }
                if (Util.IsBetween(y2, s.Y, sY2))
                {
                    if (Util.IsBetween(s.X, x, x2) && Util.DistSqr(s.X, y2, mx, my) < attachmentEpsylonSqr)
                        points.Add(new Point(s.X + canvasTranslation.X, y2 + canvasTranslation.Y));
                    if (Util.IsBetween(sX2, x, x2) && Util.DistSqr(sX2, y2, mx, my) < attachmentEpsylonSqr)
                        points.Add(new Point(sX2 + canvasTranslation.X, y2 + canvasTranslation.Y));
                }
            }
            else s.AttachPointToIntersection(mx, my, this, canvasTranslation, points);
        }

        private void Intersections(PointF A, PointF B, PointF C, PointF D, PointF l_1, PointF l_2, List<Point> ps)
        {
            Point? pt = Line.Intersection(A, B, l_1, l_2);
            if (pt.HasValue) ps.Add(pt.Value);
            pt = Line.Intersection(C, B, l_1, l_2);
            if (pt.HasValue) ps.Add(pt.Value);
            pt = Line.Intersection(A, D, l_1, l_2);
            if (pt.HasValue) ps.Add(pt.Value);
            pt = Line.Intersection(C, D, l_1, l_2);
            if (pt.HasValue) ps.Add(pt.Value);
        }

        public override string ToString()
        {
            return string.Format("Rectangle ([{0};{1}], [{2};{3}], width = {4}, height = {5})", x, y, X2, Y2, w, h);
        }

        public override void ClearStyle()
        {
            fillColor = Color.Transparent;
            lineColor = Color.Black;
            lineWidth = 2;
        }

        public override void MoveAll(int vx, int vy)
        {
            x += vx;
            y += vy;
        }

        public override Tuple<bool, int> MovePoint(int vx, int vy, int pointId)
        {
            int _vx = Math.Abs(vx), _vy = Math.Abs(vy);
            switch (pointId)
            {
                case 0:
                    w = Math.Abs(w - vx);
                    x = (vx > w) ? (x - w) : (x + vx);
                    h = Math.Abs(h - vy);
                    y = (vy > h) ? (y - h) : (y + vy);
                    return new Tuple<bool, int>(w > 1 && h > 1, _vx > w ? (_vy > h ? 2 : 1) : (_vy > h ? 3 : 0));
                case 1:
                    w = Math.Abs(w + vx);
                    if (_vx > w) x -= w;
                    h = Math.Abs(h - vy);
                    y = (vy > h) ? (y - h) : (y + vy);
                    return new Tuple<bool, int>(w > 1 && h > 1, _vx > w ? (_vy > h ? 3 : 0) : (_vy > h ? 2 : 1));
                case 2:
                    w = Math.Abs(w + vx);
                    if (_vx > w) x -= w;
                    h = Math.Abs(h + vy);
                    if (_vy > h) y -= h;
                    return new Tuple<bool, int>(w > 1 && h > 1, _vx > w ? (_vy > h ? 0 : 3) : (_vy > h ? 1 : 2));
                case 3:
                    w = Math.Abs(w - vx);
                    x = (vx > w) ? (x - w) : (x + vx);
                    h = Math.Abs(h + vy);
                    if (_vy > h) y -= h;
                    return new Tuple<bool, int>(w > 1 && h > 1, _vx > w ? (_vy > h ? 1 : 2) : (_vy > h ? 0 : 3));
            }
            return null;
        }

        public override Line[] ToLines()
        {
            return new Line[]
            {
                new Line(x, y, x + w, y, lineColor, lineWidth),
                new Line(x, y, x, y + h, lineColor, lineWidth),
                new Line(x, y + h, x + w, y + h, lineColor, lineWidth),
                new Line(x + w, y, x + w, y + h, lineColor, lineWidth)
            };
        }
    }
}
