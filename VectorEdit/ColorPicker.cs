﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VectorEdit
{
    internal partial class ColorPicker : Form
    {
        private struct HSL
        {
            public readonly int H, S, L;

            public HSL(int H, int S, int L)
            {
                this.H = H;
                this.S = S;
                this.L = L;
            }

            public static HSL FromRGB(int R, int G, int B)
            {
                double r = R / 255.0;
                double g = G / 255.0;
                double b = B / 255.0;
                double Cmax = Math.Max(r, Math.Max(g, b));
                double Cmin = Math.Min(r, Math.Min(g, b));
                double Δ = Cmax - Cmin;
                double h, s, l;
                l = (Cmax + Cmin) / 2;
                if (Util.Cmp(Δ, 0)) { h = 0; s = 0; }
                else {
                    if (Util.Cmp(Cmax, r))
                        h = 60 * ((((g - b) / Δ) + 6)%6);
                    else if (Util.Cmp(Cmax, g))
                        h = 60 * (((b - r) / Δ) + 2);
                    else h = 60 * (((r - g) / Δ) + 4);
                    s = Δ / (1 - Math.Abs(2 * l - 1));
                }
                return new HSL(Util.Clamp((int)Math.Round(h), 0, 360), Util.Clamp((int)Math.Round(100 * s), 0, 100), Util.Clamp((int)Math.Round(100 * l), 0, 100));
            }
        }

        LinearGradientBrush lgBrushColors, lgBrushGray, lgBrushLightness;

        private Point HSPos;
        private int LPos;
        private Color color;
        public Color Color
        {
            get { return color; }
            set
            {
                if (!noEvent)
                {
                    noEvent = true;
                    inputR.Value = value.R;
                    inputG.Value = value.G;
                    inputB.Value = value.B;
                    inputA.Value = EnableAlpha ? value.A : 255;
                    hsl = HSL.FromRGB(value.R, value.G, value.B);
                    inputH.Value = hsl.H;
                    inputS.Value = hsl.S;
                    inputL.Value = hsl.L;
                    HSPos = new Point((int)Math.Round(2.55 * (100 - hsl.S)), (int)Math.Round(255 * (hsl.H / 360.0)));
                    LPos = (int)(2.55 * hsl.L);
                    color = value;
                    noEvent = false;
                    RefreshPictureBoxes();
                }
            }
        }
        public bool EnableAlpha;
        private HSL hsl;
        private bool noEvent;
        bool pictureBox1_editing;
        bool pictureBox2_editing;

        public ColorPicker()
        {
            noEvent = false;
            pictureBox1_editing = false;
            pictureBox2_editing = false;
            lgBrushColors = new LinearGradientBrush(new Point(0, 0), new Point(0, 256), Color.Black, Color.Black);
            ColorBlend cb = new ColorBlend(7);
            EnableAlpha = /*false*/true;
            color = Color.Black;
            cb.Colors = new Color[]
            {
                Color.FromArgb(255, 0, 0), Color.FromArgb(255, 255, 0),
                Color.FromArgb(0, 255, 0), Color.FromArgb(0, 255, 255),
                Color.FromArgb(0, 0, 255), Color.FromArgb(255, 0, 255),
                Color.FromArgb(255, 0, 0)
            };
            cb.Positions = new float[] { 0, 0.17f, 0.33f, 0.5f, 0.67f, 0.83f, 1 };
            lgBrushColors.InterpolationColors = cb;
            lgBrushGray = new LinearGradientBrush(new Point(0, 0), new Point(256, 0), Color.FromArgb(0, 128, 128, 128), Color.FromArgb(128, 128, 128));
            lgBrushLightness = new LinearGradientBrush(new Point(0, 0), new Point(256, 0), Color.Black, Color.White);
            InitializeComponent();
            Color = Color.Black;
        }

        public new DialogResult ShowDialog()
        {
            pictureBox3.Visible = pictureBox3.Enabled = inputA.Visible = inputA.Enabled = labelA.Visible = EnableAlpha;
            if (EnableAlpha)
            {
                panel1.Top = 334;
                Height = 462;
            }
            else
            {
                panel1.Top = 304;
                Height = 432;
            }
            return base.ShowDialog();
        }

        public new DialogResult ShowDialog(IWin32Window w)
        {
            pictureBox3.Visible = pictureBox3.Enabled = inputA.Visible = inputA.Enabled = labelA.Visible = EnableAlpha;
            if (EnableAlpha)
            {
                panel1.Top = 334;
                Height = 462;
            }
            else
            {
                panel1.Top = 304;
                Height = 432;
            }
            return base.ShowDialog(w);
        }

        private void inputRGB_ValueChanged(object sender, EventArgs e)
        {
            if (!noEvent)
            {
                noEvent = true;
                int r = (int)inputR.Value, g = (int)inputG.Value, b = (int)inputB.Value, a = EnableAlpha ? (int)inputA.Value : 255;
                hsl = HSL.FromRGB(r, g, b);
                inputH.Value = hsl.H;
                inputS.Value = hsl.S;
                inputL.Value = hsl.L;
                HSPos = new Point((int)Math.Round(2.55 * (100 - hsl.S)), (int)Math.Round(255 * (hsl.H / 360.0)));
                LPos = (int)(2.55 * hsl.L);
                color = Color.FromArgb(a, r, g, b);
                noEvent = false;
                RefreshPictureBoxes();
            }
        }

        private void inputHSL_ValueChanged(object sender, EventArgs e)
        {
            if (!noEvent)
            {
                noEvent = true;
                int h = (int)inputH.Value, s = (int)inputS.Value, l = (int)inputL.Value, a = EnableAlpha ? (int)inputA.Value : 255;
                color = Util.RGBAFromHSLA(h, s, l, a / 255f);
                inputR.Value = color.R;
                inputG.Value = color.G;
                inputB.Value = color.B;
                hsl = new HSL(h, s, l);
                HSPos = new Point((int)Math.Round(2.55 * (100 - s)), (int)Math.Round(255 * (h / 360.0)));
                LPos = (int)(2.55 * l);
                noEvent = false;
                RefreshPictureBoxes();
            }
        }

        private void RefreshPictureBoxes()
        {
            pictureBox1.Refresh();
            pictureBox2.Refresh();
            if (EnableAlpha)
                pictureBox3.Refresh();
            pictureBox4.Refresh();
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.FillRectangle(lgBrushColors, 0, 0, 256, 256);
            e.Graphics.FillRectangle(lgBrushGray, 0, 0, 256, 256);
            if (hsl.L < 50) e.Graphics.FillRectangle(
                    new SolidBrush(Color.FromArgb((int)(255 * ((50 - hsl.L) / 50.0)), 0, 0, 0)),
                    0, 0, 256, 256);
            else e.Graphics.FillRectangle(
                    new SolidBrush(Color.FromArgb((int)(255 * ((hsl.L - 50) / 50.0)), 255, 255, 255)),
                    0, 0, 256, 256);
            //HSL hsl_r = new HSL((hsl.H + 180) % 360, hsl.S, 100 - hsl.L);
            //Color c = Util.RGBAFromHSL(hsl_r.H, hsl_r.S, hsl_r.L);
            Color c = Util.InvWBFromRGBA(color);
            e.Graphics.DrawEllipse(new Pen(c), HSPos.X - 3, HSPos.Y - 3, 6, 6);
            e.Graphics.DrawEllipse(new Pen(c, 2), HSPos.X - 1, HSPos.Y - 1, 2, 2);
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox4_Paint(object sender, PaintEventArgs e)
        {
            for (int i = 0; i < pictureBox4.Width; i += 12)
            {
                e.Graphics.DrawImage(Resource1.alpha_back, i, 0);
                e.Graphics.DrawImage(Resource1.alpha_back, i, 12);
            }
            e.Graphics.FillRectangle(new SolidBrush(color), 0, 0, pictureBox4.Width, pictureBox4.Height);
        }


        private void pictureBox2_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                pictureBox2_editing = true;
                LPos = Util.Clamp(e.X, 0, 255);
                hsl = new HSL(hsl.H, hsl.S, (int)(LPos / 2.55));
                EditHSL();
            }
            pictureBox1_editing = false;
        }

        private void pictureBox2_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
                pictureBox2_editing = false;
        }

        private void pictureBox2_MouseLeave(object sender, EventArgs e)
        {
            pictureBox2_editing = false;
        }

        private void pictureBox2_MouseMove(object sender, MouseEventArgs e)
        {
            if (pictureBox2_editing)
            {
                LPos = Util.Clamp(e.X, 0, 255);
                hsl = new HSL(hsl.H, hsl.S, (int)(LPos / 2.55));
                EditHSL();
            }
        }

        private void EditHSL()
        {
            noEvent = true;
            int a = EnableAlpha ? (int)inputA.Value : 255;
            color = Util.RGBAFromHSLA(hsl.H, hsl.S, hsl.L, a / 255f);
            inputR.Value = color.R;
            inputG.Value = color.G;
            inputB.Value = color.B;
            inputH.Value = hsl.H;
            inputS.Value = hsl.S;
            inputL.Value = hsl.L;
            noEvent = false;
            RefreshPictureBoxes();
        }

        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                pictureBox1_editing = true;
                HSPos = new Point(Util.Clamp(e.X, 0, 255), Util.Clamp(e.Y, 0, 255));
                hsl = new HSL((int)(HSPos.Y / 255.0 * 360), (int)((255 - HSPos.X) / 2.55), hsl.L);
                EditHSL();
            }
            pictureBox2_editing = false;
        }

        private void pictureBox1_MouseLeave(object sender, EventArgs e)
        {
            pictureBox1_editing = false;
        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            if (pictureBox1_editing)
            {
                HSPos = new Point(Util.Clamp(e.X, 0, 255), Util.Clamp(e.Y, 0, 255));
                hsl = new HSL((int)(HSPos.Y / 255.0 * 360), (int)((255 - HSPos.X) / 2.55), hsl.L);
                EditHSL();
            }
        }

        private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
                pictureBox1_editing = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void inputA_ValueChanged(object sender, EventArgs e)
        {
            inputRGB_ValueChanged(sender, e);
        }

        private void pictureBox2_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.FillRectangle(lgBrushLightness, 0, 0, 256, 24);
            e.Graphics.FillRectangle(new SolidBrush(hsl.L > 50 ? Color.Black : Color.White), LPos - 1, 0, 2, 24);
            //e.Graphics.FillRectangle(Brushes.Lime, LPos - 1, 0, 2, 24);
        }

        private void pictureBox3_Paint(object sender, PaintEventArgs e)
        {
            for (int i = 0; i < pictureBox3.Width; i += 12)
            {
                e.Graphics.DrawImage(Resource1.alpha_back, i, 0);
                e.Graphics.DrawImage(Resource1.alpha_back, i, 12);
            }
            e.Graphics.FillRectangle(new SolidBrush(color), 0, 0, pictureBox3.Width, pictureBox3.Height);
        }
    }
}
