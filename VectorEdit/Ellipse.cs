﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VectorEdit
{
    /// <summary>
    /// Ellipse shape class
    /// </summary>
    internal class Ellipse : Shape
    {
        public int cx { get; set; }
        public int cy { get; set; }
        public int a { get; set; }
        public int b { get; set; }
        public override int X { get { return cx - a; } }
        public override int Y { get { return cy - b; } }
        public int w { get { return a * 2; } }
        public int h { get { return b * 2; } }
        public override int X2 { get { return cx + a; } }
        public override int Y2 { get { return cy + b; } }

        public Color lineColor, fillColor;

        public byte lineWidth;
        byte lineWidth_d2 { get { return (byte)(lineWidth / 2); } }

        public Ellipse(int x1, int y1, int x2, int y2, Color color, byte lineWidth = 2)
        {
            cx = x1;
            cy = y1;
            a = Math.Abs(x1 - x2);
            b = Math.Abs(y1 - y2);
            lineColor = color;
            fillColor = Color.Transparent;
            this.lineWidth = lineWidth;
        }

        public Ellipse(BinaryReader br)
        {
            cx = br.ReadInt32();
            cy = br.ReadInt32();
            a = br.ReadInt32();
            b = br.ReadInt32();
            fillColor = Color.FromArgb(br.ReadByte(), br.ReadByte(), br.ReadByte(), br.ReadByte());
            lineColor = Color.FromArgb(br.ReadByte(), br.ReadByte(), br.ReadByte());
            lineWidth = br.ReadByte();
        }

        public override void Render(Graphics g, Point canvasTranslation)
        {
            g.FillEllipse(new SolidBrush(fillColor),
                X + canvasTranslation.X, Y + canvasTranslation.Y, w, h);
            g.DrawEllipse(new Pen(lineColor, lineWidth),
                X + canvasTranslation.X, Y + canvasTranslation.Y, w, h);
        }

        public override void RenderEditing(Graphics g, Point canvasTranslation, int time)
        {
            int _x = X + canvasTranslation.X, _y = Y + canvasTranslation.Y;
            int _x2 = X2 + canvasTranslation.X, _y2 = Y2 + canvasTranslation.Y;
            int a = (int)Math.Round(255 * ((1 + Math.Cos(Math.PI * time / 40.0)) / 2));
            if (a > 0) {
                g.FillEllipse(new SolidBrush(Color.FromArgb((int)(a * (fillColor.A / 255.0)), fillColor.R, fillColor.G, fillColor.B)), _x, _y, w, h);
                g.DrawEllipse(new Pen(Color.FromArgb(a, lineColor.R, lineColor.G, lineColor.B), lineWidth), _x, _y, w, h);
            }
            g.DrawEllipse(new Pen(new HatchBrush(HatchStyle.LargeCheckerBoard, Color.FromArgb(255 - a, 0, 0, 255), Color.Transparent)), _x, _y, w, h);
            RenderEditingPoint(g, new Point(_x, _y));
            RenderEditingPoint(g, new Point(_x2, _y2));
            RenderEditingPoint(g, new Point(_x2, _y));
            RenderEditingPoint(g, new Point(_x, _y2));
            RenderEditingPoint(g, new Point(cx + canvasTranslation.X, cy + canvasTranslation.Y));
        }

        public override Point[] EditingPoints()
        {
            return new Point[] { new Point(cx, cy), new Point(X, Y), new Point(X2, Y), new Point(X2, Y2), new Point(X, Y2) };
        }

        public override string RenderSVG(Point canvasTranslation)
        {
            if (a == b)
            {
                return string.Format("<circle cx=\"{0}\" cy=\"{1}\" r=\"{2}\" style=\"fill: #{3:x2}{4:x2}{5:x2}; stroke: #{6:x2}{7:x2}{8:x2}; stroke-width: {9}px; fill-opacity: {10};\" />",
                    cx + canvasTranslation.X, cy + canvasTranslation.Y, a, fillColor.R, fillColor.G, fillColor.B, lineColor.R, lineColor.G, lineColor.B, lineWidth, fillColor.A / 255.0);
            }
            else
            {
                return string.Format("<ellipse cx=\"{0}\" cy=\"{1}\" rx=\"{2}\" ry=\"{3}\" style=\"fill: #{4:x2}{5:x2}{6:x2}; stroke: #{7:x2}{8:x2}{9:x2}; stroke-width: {10}px; fill-opacity: {11};\" />",
                    cx + canvasTranslation.X, cy + canvasTranslation.Y, a, b, fillColor.R, fillColor.G, fillColor.B, lineColor.R, lineColor.G, lineColor.B, lineWidth, fillColor.A / 255.0);
            }
        }

        public override void SaveBinary(BinaryWriter bw)
        {
            bw.Write(ShapeToId(GetType()));
            bw.WriteR(cx).WriteR(cy).WriteR(a).Write(b);
            bw.WriteR(fillColor.A).WriteR(fillColor.R).WriteR(fillColor.G).Write(fillColor.B);
            bw.WriteR(lineColor.R).WriteR(lineColor.G).Write(lineColor.B);
            bw.Write(lineWidth);
        }

        public static void PreRender(Graphics g, int x1, int y1, int x2, int y2)
        {
            int _a = Math.Abs(x1 - x2), _b = Math.Abs(y1 - y2);
            g.DrawEllipse(preRenderPen, x1 - _a, y1 - _b, _a * 2, _b * 2);
        }

        public override bool Hit(int mx, int my, Point canvasTranslation)
        {
            int lwd2 = lineWidth_d2 + 2;
            mx -= canvasTranslation.X;
            my -= canvasTranslation.Y;
            if (a == b)
            {
                double d = Math.Sqrt((mx - cx) * (mx - cx) + (my - cy) * (my - cy));
                return (d <= a + lwd2) && (d >= a - lwd2);
            }
            return IsInside(mx, my, a + lwd2, b + lwd2) && !IsInside(mx, my, a - lwd2, b - lwd2);
        }

        public override void AttachPointP(int mx, int my, Point canvasTranslation, List<Point> points)
        {
            mx -= canvasTranslation.X;
            my -= canvasTranslation.Y;
            if (((cx - mx) * (cx - mx) + (cy - my) * (cy - my)) < attachmentEpsylonSqr)
                points.Add(new Point(cx + canvasTranslation.X, cy + canvasTranslation.Y));
        }

        public override void AttachPointS(int mx, int my, Point canvasTranslation, List<Point> points)
        {
            mx -= canvasTranslation.X;
            my -= canvasTranslation.Y;
            int lwd2 = lineWidth_d2 + 2;
            if (a == b)
            {
                double xdif = mx - cx, ydif = my - cy;
                double d = Math.Sqrt(xdif * xdif + ydif * ydif);
                if (Math.Abs(d - a) < attachmentEpsylon)
                    points.Add(new Point(cx + (int)Math.Round(xdif / d * a) + canvasTranslation.X,
                        cy + (int)Math.Round(ydif / d * a) + canvasTranslation.Y));
            }
            else if (IsInside(mx, my, a + attachmentEpsylon, b + attachmentEpsylon) && !IsInside(mx, my, a - attachmentEpsylon, b - attachmentEpsylon))
                points.Add(MoveToEdge(mx, my, canvasTranslation));
        }

        public override void AttachPointToIntersection(int mx, int my, Shape s, Point canvasTranslation, List<Point> points)
        {
            if (s is Ellipse)
            {
                mx -= canvasTranslation.X;
                my -= canvasTranslation.Y;

            } else s.AttachPointToIntersection(mx, my, this, canvasTranslation, points);
        }

        private bool IsInside(int mx, int my, double _a, double _b)
        {
            double _x = (mx - cx) / _a, _y = (my - cy) / _b;
            return (Math.Sqrt(_x * _x + _y * _y) <= 1);
        }

        public Point MoveToEdge(int mx, int my, Point canvasTranslation)
        {
            double _x = (mx - cx) / (double)a, _y = (my - cy) / (double)b;
            double d = Math.Sqrt(_x * _x + _y * _y);
            return new Point((int)Math.Round(cx + (_x / d * a)) + canvasTranslation.X, (int)Math.Round(cy + (_y / d * b)) + canvasTranslation.Y);
        }

        public Point MoveToEdge(int mx, int my)
        {
            double _x = (mx - cx) / (double)a, _y = (my - cy) / (double)b;
            double d = Math.Sqrt(_x * _x + _y * _y);
            return new Point((int)Math.Round(cx + (_x / d * a)), (int)Math.Round(cy + (_y / d * b)));
        }

        public override string ToString()
        {
            return string.Format("Ellipse (S[{0};{1}], a = {2}, b = {3})", cx, cy, a, b);
        }

        /// <param name="lineCount">36 &lt;= lineCount &lt;= 360</param>
        public PointF[] ToPolyline(int lineCount)
        {
            /*if (lineCount < 36)
                throw new ArgumentOutOfRangeException("lineCount");*/
            if (lineCount > 360)
                throw new ArgumentOutOfRangeException("lineCount");
            PointF[] points = new PointF[lineCount];
            float f = (float)(Math.PI * 2) / lineCount, f2;
            for (int i = 0; i < lineCount; i++)
            {
                f2 = i * f;
                points[i] = new PointF((float)(cx + a * Math.Sin(f2)), (float)(cy + b * Math.Cos(f2)));
            }
            return points;
        }

        public override void ClearStyle()
        {
            fillColor = Color.Transparent;
            lineColor = Color.Black;
            lineWidth = 2;
        }

        public override void MoveAll(int vx, int vy)
        {
            cx += vx;
            cy += vy;
        }
        
        public override Tuple<bool, int> MovePoint(int vx, int vy, int pointId)
        {
            switch (pointId)
            {
                case 0:
                    cx += vx;
                    cy += vy;
                    return new Tuple<bool, int>(true, 0);
                case 1:
                    a = Math.Abs(a - vx);
                    b = Math.Abs(b - vy);
                    return new Tuple<bool, int>(a >= 1 && b >= 1, vx > a ? (vy > b ? 3 : 2) : (vy > b ? 4 : 1));
                case 2:
                    a = Math.Abs(a + vx);
                    b = Math.Abs(b - vy);
                    return new Tuple<bool, int>(a >= 1 && b >= 1, vx > a ? (vy > b ? 4 : 1) : (vy > b ? 3 : 2));
                case 3:
                    a = Math.Abs(a + vx);
                    b = Math.Abs(b + vy);
                    return new Tuple<bool, int>(a >= 1 && b >= 1, vx > a ? (vy > b ? 1 : 4) : (vy > b ? 2 : 3));
                case 4:
                    a = Math.Abs(a - vx);
                    b = Math.Abs(b + vy);
                    return new Tuple<bool, int>(a >= 1 && b >= 1, vx > a ? (vy > b ? 2 : 3) : (vy > b ? 1 : 4));
            }
            return null;
        }

        public override Line[] ToLines()
        {
            PointF[] ps = ToPolyline(Math.Min(360, Math.Max((int)Math.Round(Math.Sqrt(a * a + b * b) / 6), 4)));
            Line[] ls = new Line[ps.Length];
            for (int i = 0; i < ls.Length; i++)
            {
                PointF p = ps[(i + 1) % ls.Length];
                ls[i] = new Line((int)Math.Round(ps[i].X), (int)Math.Round(ps[i].Y),
                    (int)Math.Round(p.X), (int)Math.Round(p.Y), lineColor, lineWidth);
            }
            return ls;
        }
    }
}
