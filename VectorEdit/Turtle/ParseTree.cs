﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace VectorEdit.Turtle
{
    class ParseTree
    {
        public abstract class Branch
        {
            public readonly Branch[] brs;

            protected Branch(Branch[] brs)
            {
                this.brs = brs;
            }

            public abstract ProcessableValue Evaluate(Turtle t);

            public abstract ValueType ValueType { get; }

            public override string ToString()
            {
                return "?";
            }
        }

        public enum OperatorType { Add, Sub, Mul, Div, Mod }

        public class Operator : Branch
        {
            OperatorType ot;


            public Operator(Branch left, Branch right, OperatorType type) : base(new Branch[] { left, right })
            {
                if (left.ValueType == ValueType.NAV)
                    TurtleInterpreter.LogError("Operator can work only with values!");
                if (right.ValueType == ValueType.NAV)
                    TurtleInterpreter.LogError("Operator can work only with values!");
                ot = type;
            }

            public override ProcessableValue Evaluate(Turtle t)
            {
                switch (ot)
                {
                    case OperatorType.Add: return brs[0].Evaluate(t) + brs[1].Evaluate(t);
                    case OperatorType.Sub: return brs[0].Evaluate(t) - brs[1].Evaluate(t);
                    case OperatorType.Mul: return brs[0].Evaluate(t) * brs[1].Evaluate(t);
                    case OperatorType.Div: return brs[0].Evaluate(t) / brs[1].Evaluate(t);
                    case OperatorType.Mod: return brs[0].Evaluate(t) % brs[1].Evaluate(t);
                    default: return new ProcessableValue();
                }
            }

            public override string ToString()
            {
                switch (ot)
                {
                    case OperatorType.Add: return "( " + brs[0] + " + " + brs[1] + " )";
                    case OperatorType.Sub: return "( " + brs[0] + " - " + brs[1] + " )";
                    case OperatorType.Mul: return "( " + brs[0] + " * " + brs[1] + " )";
                    case OperatorType.Div: return "( " + brs[0] + " / " + brs[1] + " )";
                    case OperatorType.Mod: return "( " + brs[0] + " % " + brs[1] + " )";
                    default: return "?";
                }
            }

            public override ValueType ValueType
            {
                get { return ValueType.Any; }
            }
        }

        public class Value : Branch
        {
            float val;

            public Value(float val) : base(new Branch[0])
            {
                this.val = val;
            }

            public override ProcessableValue Evaluate(Turtle t)
            {
                return new ProcessableValue(val);
            }

            public override ValueType ValueType
            {
                get { return ValueType.Number; }
            }

            public override string ToString()
            {
                return "" + val;
            }
        }

        public class ColorValue : Branch
        {
            Color val;

            public ColorValue(Color val) : base(new Branch[0])
            {
                this.val = val;
            }

            public override ProcessableValue Evaluate(Turtle t)
            {
                return new ProcessableValue(val);
            }

            public override ValueType ValueType
            {
                get { return ValueType.Color; }
            }

            public override string ToString()
            {
                return "" + val;
            }
        }

        public class BoolValue : Branch
        {
            bool val;

            public BoolValue(bool val) : base(new Branch[0])
            {
                this.val = val;
            }

            public override ProcessableValue Evaluate(Turtle t)
            {
                return new ProcessableValue(val);
            }

            public override ValueType ValueType
            {
                get { return ValueType.Number; }
            }

            public override string ToString()
            {
                return "" + val;
            }
        }

        public class Group : Branch
        {
            public Group(Branch[] branches) : base(branches) { }

            public override ProcessableValue Evaluate(Turtle t)
            {
                foreach (Branch br in brs) br.Evaluate(t);
                return new ProcessableValue();
            }

            public override ValueType ValueType
            {
                get { return ValueType.NAV; }
            }

            public override string ToString()
            {
                return "Group";
            }
        }

        public class Loop : Branch
        {
            public Loop(Branch count, Branch target) : base(new Branch[] { count, target }) { }

            public override ProcessableValue Evaluate(Turtle t)
            {
                float f = brs[0].Evaluate(t).SafeGetNumber();
                for (int i = 0; i < f; i++) brs[1].Evaluate(t);
                return new ProcessableValue();
            }

            public override ValueType ValueType
            {
                get { return ValueType.NAV; }
            }

            public override string ToString()
            {
                return "Repeat loop";
            }
        }

        public enum ValueType
        {
            Any,
            Number, // number       (float)
            Color,  // color        (Color)
            Bool,   // boolean      (boolean)
            NAV     // not a value
        }

        public class Variable : Branch
        {
            char name;


            public Variable(char name) : base(new Branch[0])
            {
                this.name = name;
            }

            public override ProcessableValue Evaluate(Turtle t)
            {
                ProcessableValue pv;
                if (!vars.TryGetValue(name, out pv))
                {
                    TurtleInterpreter.LogError("Variable " + name + " doesn't exist!");
                    return new ProcessableValue();
                }
                return pv;
            }

            public override ValueType ValueType
            {
                get { return ValueType.Number; }
            }

            public override string ToString()
            {
                return "" + name;
            }
        }

        public class Command : Branch
        {
            private char name;

            public Command(char name, Branch[] args) : base(args == null ? new Branch[0] : args)
            {
                this.name = name;
            }

            private bool CheckArgs(int min, ValueType[] argTypes)
            {
                if (argTypes == null) return false;
                if (brs.Length < min)
                {
                    TurtleInterpreter.LogError("Command " + name + " needs at least " + min + "arguments (" + brs.Length + " provided)!");
                    return false;
                }
                if (brs.Length > argTypes.Length)
                {
                    TurtleInterpreter.LogError("Command " + name + " can have only " + argTypes.Length + "arguments (" + brs.Length + " provided)!");
                    return false;
                }
                for (int i = 0; i < brs.Length; i++)
                {
                    if (brs[i].ValueType == ValueType.Any) continue;
                    switch (argTypes[i])
                    {
                        case ValueType.NAV: return false;
                        case ValueType.Number:
                            if (brs[i].ValueType != ValueType.Number && brs[i].ValueType != ValueType.Bool)
                            {
                                TurtleInterpreter.LogError("Command " + name + " must have a number as it's " + (i + 1) + (i == 0 ? "st" : (i == 1 ? "nd" : (i == 2 ? "rd" : "th"))) + " argument!");
                                return false;
                            }
                            break;
                        case ValueType.Bool:
                            if (brs[i].ValueType != ValueType.Bool)
                            {
                                TurtleInterpreter.LogError("Command " + name + " must have boolean as it's " + (i + 1) + (i == 0 ? "st" : (i == 1 ? "nd" : (i == 2 ? "rd" : "th"))) + " argument!");
                                return false;
                            }
                            break;
                        case ValueType.Color:
                            if (brs[i].ValueType != ValueType.Color)
                            {
                                TurtleInterpreter.LogError("Command " + name + " must have color as it's " + (i + 1) + (i == 0 ? "st" : (i == 1 ? "nd" : (i == 2 ? "rd" : "th"))) + " argument!");
                                return false;
                            }
                            break;
                    }
                }
                return true;
            }

            public override ProcessableValue Evaluate(Turtle t)
            {
                switch (name)
                {
                    case 'M':
                        if (CheckArgs(2, new ValueType[] { ValueType.Number, ValueType.Number }))
                        {
                            float x = brs[0].Evaluate(t).SafeGetNumber();
                            float y = brs[0].Evaluate(t).SafeGetNumber();
                            vars.Remove('x');
                            vars.Add('x', new ProcessableValue(t.x = x));
                            vars.Remove('y');
                            vars.Add('y', new ProcessableValue(t.y = y));
                        }
                        break;
                    case 'F':
                    case 'B':
                        if (CheckArgs(1, new ValueType[] { ValueType.Number, ValueType.Bool }))
                        {
                            float dist = brs[0].Evaluate(t).SafeGetNumber();
                            bool draw = brs.Length > 1 ? brs[1].Evaluate(t).SafeGetBool(true) : true;
                            float x = t.x, y = t.y;
                            t.Forward((name == 'B' ? -1 : 1) * dist);
                            if (draw && Util.Dist((int)Math.Round(x), (int)Math.Round(y), (int)Math.Round(t.x), (int)Math.Round(t.y)) > 0)
                                TurtleInterpreter.Lines.Add(new Line((int)Math.Round(x),
                                    (int)Math.Round(y), (int)Math.Round(t.x),
                                    (int)Math.Round(t.y), vars['c'].SafeGetColor()));
                        }
                        break;
                    case 'L':
                    case 'R':
                        if (CheckArgs(1, new ValueType[] { ValueType.Number }))
                        {
                            float _a = brs[0].Evaluate(t).SafeGetNumber();
                            t.a += (name == 'R' ? -1 : 1) * _a;
                            vars.Remove('a');
                            vars.Add('a', new ProcessableValue(t.a));
                        }
                        break;
                    case 'U':
                        if (CheckArgs(0, new ValueType[0]))
                        {
                            if (TurtleInterpreter.Stack.Count >= 1024)
                            {
                                TurtleInterpreter.LogError("Stack is full!");
                                break;
                            }
                            TurtleInterpreter.Stack.Push(new Turtle(t));
                        }
                        break;
                    case 'O':
                        if (CheckArgs(0, new ValueType[0]))
                        {
                            if (TurtleInterpreter.Stack.Count == 0)
                            {
                                TurtleInterpreter.LogError("Can't pop from empty stack!");
                                break;
                            }
                            t.Set(TurtleInterpreter.Stack.Pop());
                        }
                        break;
                    case 'C':
                        if (CheckArgs(1, new ValueType[] { ValueType.Color }))
                        {
                            vars.Remove('c');
                            vars.Add('c', new ProcessableValue(brs[0].Evaluate(t).SafeGetColor()));
                        }
                        break;
                    default:
                        TurtleInterpreter.LogError("Command " + name + " doesn't exist!");
                        break;
                }
                return new ProcessableValue();
            }

            public override ValueType ValueType
            {
                get { return ValueType.NAV; }
            }

            public override string ToString()
            {
                return "function " + name;
            }
        }

        public readonly Branch top;

        private static Dictionary<char, ProcessableValue> vars;

        static ParseTree()
        {
            vars = new Dictionary<char, ProcessableValue>();
            vars.Add('p', new ProcessableValue((float)Math.PI));
            vars.Add('e', new ProcessableValue((float)Math.E));
        }

        public ParseTree(Branch top)
        {
            this.top = top;
        }

        public void Evaluate(Turtle t)
        {
            vars.Remove('x');
            vars.Add('x', new ProcessableValue(t.x));
            vars.Remove('y');
            vars.Add('y', new ProcessableValue(t.y));
            vars.Remove('a');
            vars.Add('a', new ProcessableValue(t.a));
            vars.Remove('c');
            vars.Add('c', new ProcessableValue(Color.Black));
            top.Evaluate(t);
        }

        public override string ToString()
        {
            return top.ToString();
        }
    }
}
