﻿using System;
using System.Drawing;

namespace VectorEdit.Turtle
{
    internal class EChar
    {
        public readonly float f;
        public readonly Color col;
	    public readonly char s;
	    public readonly char c;
        public readonly EChar[] brs;
        public readonly bool isInParantheses;

        //ⓘ

        public EChar(char c)
        {
            f = 0;
            s = '0';
            col = Color.Black;
            this.c = c;
            isInParantheses = false;
            brs = new EChar[0];
        }

        public EChar(float f)
        {
            this.f = f;
            s = '0';
            col = Color.Black;
            isInParantheses = false;
            c = 'ⓝ';
            brs = new EChar[0];
        }

        public EChar(bool b)
        {
            f = b ? 1 : 0;
            s = '0';
            col = Color.Black;
            isInParantheses = false;
            c = 'ⓑ';
            brs = new EChar[0];
        }

        public EChar(char s, bool b)
        {
            f = 0;
            this.s = s;
            col = Color.Black;
            isInParantheses = false;
            c = 'ⓢ';
            brs = new EChar[0];
        }

        public EChar(Color col)
        {
            f = 0;
            s = '0';
            this.col = col;
            isInParantheses = false;
            c = 'ⓒ';
            brs = new EChar[0];
        }

        public EChar(char s, params EChar[] args)
        {
            f = 0;
            this.s = s;
            col = Color.Black;
            c = 'ⓕ';
            isInParantheses = false;
            brs = args;
        }

        public EChar(EChar[] branches)
        {
            f = 0;
            s = '0';
            col = Color.Black;
            isInParantheses = false;
            brs = branches == null ? new EChar[0] : branches;
            c = 'ⓔ';
        }

        public EChar(EChar[] branches, char c)
        {
            f = 0;
            s = '0';
            col = Color.Black;
            isInParantheses = false;
            brs = branches == null ? new EChar[0] : branches;
            this.c = c;
        }

        public EChar(float f, char s, Color col, EChar[] branches, char c, bool isInParantheses)
        {
            this.f = f;
            this.s = s;
            this.col = col;
            brs = branches == null ? new EChar[0] : branches;
            this.c = c;
            this.isInParantheses = isInParantheses;
        }

        public EChar withChar(char c, bool isInParantheses = false)
        {
            return new EChar(f, s, col, brs, c, isInParantheses);
        }

        public ParseTree.Branch simplify()
        {
            if (c == 'ⓔ')
            {
                if (brs.Length == 3)
                {
                    switch (brs[1].c)
                    {
                        case '+':
                            return new ParseTree.Operator(
                          brs[0].simplify(), brs[2].simplify(),
                          ParseTree.OperatorType.Add);
                        case '-':
                            return new ParseTree.Operator(
                          brs[0].simplify(), brs[2].simplify(),
                          ParseTree.OperatorType.Sub);
                        case '*':
                            return new ParseTree.Operator(
                          brs[0].simplify(), brs[2].simplify(),
                          ParseTree.OperatorType.Mul);
                        case '/':
                            return new ParseTree.Operator(
                          brs[0].simplify(), brs[2].simplify(),
                          ParseTree.OperatorType.Div);
                        case '%':
                            return new ParseTree.Operator(
                          brs[0].simplify(), brs[2].simplify(),
                          ParseTree.OperatorType.Mod);
                    }
                }
            }
            else if (c == 'ⓝ')
                return new ParseTree.Value(f);
            else if (c == 'ⓑ')
                return new ParseTree.BoolValue(f > 0);
            else if (c == 'ⓒ')
                return new ParseTree.ColorValue(col);
            else if (c == '◯')
            {
                ParseTree.Branch[] mbrs = new ParseTree.Branch[brs.Length];
                for (int i = 0; i < brs.Length; i++)
                    mbrs[i] = brs[i].simplify();
                return new ParseTree.Group(mbrs);
            }
            else if (c == 'ⓛ')
                return new ParseTree.Loop(brs[0].simplify(), brs[1].simplify());
            else if (c == 'ⓕ')
            {
                ParseTree.Branch[] mbrs = new ParseTree.Branch[brs.Length];
                for (int i = 0; i < brs.Length; i++)
                    mbrs[i] = brs[i].simplify();
                return new ParseTree.Command(s, mbrs);
            }
            else if (c == 'ⓢ')
            {
                return new ParseTree.Variable(s);
            }
            return null;
        }
    }

}
