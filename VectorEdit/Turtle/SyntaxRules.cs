﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//////  M(256,256)Y(360){F(3)R(1)}
//////  C(#ff0000)M(512,256)Y(360){F(3)R(1)}
namespace VectorEdit.Turtle
{
    class SyntaxRules
    {
        public class Rule
        {
            public delegate bool Check(List<EChar> ca, int pos);

            public delegate void Replace(List<EChar> ca, int pos);

            public Check check { get; protected set; }

            public Replace replace { get; protected set; }

            public int priority;

            public Rule(Check check, Replace replace, int priority)
            {
                this.check = check;
                this.replace = replace;
                this.priority = priority;
            }

            public static char Get(List<EChar> ca, int pos)
            {
                if (pos < 0) return (char)2;
                if (pos >= ca.Count) return (char)3;
                EChar c = ca[pos];
                if (c == null) return ' ';
                return c.c;
            }
            public static bool IsN(List<EChar> ca, int pos)
            {
                char c = Get(ca, pos);
                return c == 'ⓝ' || c == 'ⓑ' || c == 'ⓒ';
            }
            public static bool IsIorN(List<EChar> ca, int pos)
            {
                char c = Get(ca, pos);
                return c == 'ⓢ' || c == 'ⓝ' || c == 'ⓑ' || c == 'ⓒ';
            }
            public static bool IsEorN(List<EChar> ca, int pos)
            {
                char c = Get(ca, pos);
                return c == 'ⓔ' || c == 'ⓝ';
            }
            public static bool IsIEN(List<EChar> ca, int pos)
            {
                char c = Get(ca, pos);
                return c == 'ⓢ' || c == 'ⓔ' || c == 'ⓝ' || c == 'ⓑ' || c == 'ⓒ';
            }
        }

        public class OperatorRule : Rule
        {
            private char _operator;

            public OperatorRule(char _operator, int priority) : base(null, null, priority)
            {
                this._operator = _operator;
                check = _check;
                replace = _replace;
            }

            private bool _check(List<EChar> ca, int pos)
            {
                return (IsIEN(ca, pos) && Get(ca, ++pos) == _operator && IsIEN(ca, ++pos));
            }

            private void _replace(List<EChar> ca, int pos)
            {
                EChar[] cs = new EChar[] {
                    ca[pos], ca[pos + 1], ca[pos + 2]
                };
                for (int i = 0; i < 3; i++) ca.RemoveAt(pos);
                ca.Insert(pos, new EChar(cs));
            }
        }

        private static Rule[] rules;

        private static string colorDefChars = "0123456789abcdef";

        static SyntaxRules()
        {
            rules = new Rule[] {
                new OperatorRule('+', 10),
                new OperatorRule('-', 10),
                new OperatorRule('*', 11),
                new OperatorRule('/', 11),
                new OperatorRule('%', 11),
                new Rule(   //  { command command ... }
                    (List<EChar> ca, int pos) =>
                    {
                        if (!(Rule.Get(ca, pos) == '{')) return false;
                        int i = 0;
                        for (pos++; pos < ca.Count; pos++)
                        {
                            char c = Rule.Get(ca, pos);
                            if (c == '{') i++;
                            else if (c == '}')
                            {
                                if (i == 0) return true;
                                i--;
                            }
                        }
                        return false;
                    },
                    (List<EChar> ca, int pos) =>
                    {
                        int p = pos, p2 = -1;
                        int i = 0;
                        char c;
                        for (pos++; pos < ca.Count; pos++)
                        {
                            c = Rule.Get(ca, pos);
                            if (c == '{') i++;
                            else if (c == '}')
                            {
                                if (i-- == 0)
                                {
                                    p2 = ++pos;
                                    break;
                                }
                            }
                        }
                        if (p2 > 0)
                        {
                            List<EChar> l = new List<EChar>();
                            for (i = p; i < p2; i++)
                            {
                                if (i > p && i < (p2 - 1))
                                    l.Add(ca[p]);
                                ca.RemoveAt(p);
                            }
                            if (p2 - p > 2) ca.Insert(p, new EChar(SyntaxRules.Build(l), '◯'));
                        }
                    }, 20),
                new Rule(   //  ( E )
                    (List<EChar> ca, int pos) =>
                    {
                        if (!(Rule.Get(ca, pos) == '(')) return false;
                        int i = 0;
                        for (pos++; pos < ca.Count; pos++)
                        {
                            char c = Rule.Get(ca, pos);
                            if (c == '(') i++;
                            else if (c == ')')
                            {
                                if (i == 0) return true;
                                i--;
                            }
                            else if (c == ',') return false;;
                        }
                        return false;
                    },
                    (List<EChar> ca, int pos) =>
                    {
                        int p = pos, p2 = -1;
                        int i = 0;
                        char c;
                        for (pos++; pos < ca.Count; pos++)
                        {
                            c = Rule.Get(ca, pos);
                            if (c == '(') i++;
                            else if (c == ')')
                            {
                                if (i-- == 0)
                                {
                                    p2 = ++pos;
                                    break;
                                }
                            }
                            else if (c == ',') break;
                        }
                        if (p2 > 0)
                        {
                            List<EChar> l = new List<EChar>();
                            for (i = p; i < p2; i++)
                            {
                                if (i > p && i < (p2 - 1))
                                    l.Add(ca[p]);
                                ca.RemoveAt(p);
                            }
                            if (p2 - p > 2)
                                ca.Insert(p, Build(l, true)[0].withChar('ⓔ', true));
                        }
                    }, 15),
                new Rule(   //  F
                    (List<EChar> ca, int pos) =>
                    {
                        return char.IsUpper(Rule.Get(ca, pos));
                    },
                    (List<EChar> ca, int pos) =>
                    {
                        char ch = Rule.Get(ca, pos);
                        pos++;
                        List<EChar> args = new List<EChar>();
                        if (pos < ca.Count - 1)
                        {
                            int p = pos, p2 = -1;
                            int i = 0;
                            char c;
                            for (pos++; pos < ca.Count; pos++)
                            {
                                c = Rule.Get(ca, pos);
                                if (c == '(') i++;
                                else if (c == ')')
                                {
                                    if (i-- == 0)
                                    {
                                        p2 = ++pos;
                                        break;
                                    }
                                }
                            }
                            if (p2 > 0)
                            {
                                List<EChar> l = new List<EChar>();
                                for (i = p; i < p2; i++)
                                {
                                    if (i > p && i < (p2 - 1))
                                        l.Add(ca[p]);
                                    ca.RemoveAt(p);
                                }
                                if (p2 - p > 2) args.AddRange(Build(l, true));
                            }
                            ca[--p] = new EChar(ch, args.ToArray());
                        }
                    }, 18),
                new Rule(   // loop
                    (List<EChar> ca, int pos) => {
                        return Rule.Get(ca, pos) == 'ⓕ' && ca[pos].s == 'Y'; },
                    (List<EChar> ca, int pos) =>
                    {
                        char c = Rule.Get(ca, pos + 1);
                        if (c == 'ⓕ' || c == '◯')
                        {
                            if (ca[pos].brs.Length != 1)
                            {
                                TurtleInterpreter.LogError("Invalid loop definition!");
                                return;
                            }
                            EChar ec = new EChar(new EChar[] { ca[pos].brs[0], ca[pos + 1] }, 'ⓛ');
                            ca.RemoveAt(pos);
                            ca[pos] = ec;
                        }
                        else TurtleInterpreter.LogError("Invalid loop definition!");
                    }, 17),
                /*new Rule(   // interval
                    (List<EChar> ca, int pos) => { return Rule.Get(ca, pos) == 'Y'; },
                    (List<EChar> ca, int pos) =>
                    { ca[pos] = new EChar(Rule.Get(ca, pos), true); }, 17),*/
                new Rule(   //  N
                    (List<EChar> ca, int pos) => { return char.IsDigit(Rule.Get(ca, pos)); },
                    (List<EChar> ca, int pos) =>
                    {
                        string s = "";
                        while (true)
                        {
                            char c = Rule.Get(ca, pos);
                            if (!(char.IsDigit(c) || (s.Length > 0 && c == '.'))) break;
                            s += c;
                            ca.RemoveAt(pos);
                        }
                        if (s.Length > 0)
                        {
                            float f;
                            if (float.TryParse(s, out f))
                                ca.Insert(pos, new EChar(f));
                        }
                    }, 100),
                new Rule(   //  I
                    (List<EChar> ca, int pos) => { return char.IsLower(Rule.Get(ca, pos)); },
                    (List<EChar> ca, int pos) =>
                    { ca[pos] = new EChar(Rule.Get(ca, pos), true); }, 99),
                new Rule(   //  B
                    (List<EChar> ca, int pos) =>
                    {
                        char c = Rule.Get(ca, pos);
                        return c == 't' || c == 'f';
                    },
                    (List<EChar> ca, int pos) =>
                    { ca[pos] = new EChar(Rule.Get(ca, pos) == 't'); }, 100),
                new Rule(   //  C
                    (List<EChar> ca, int pos) => { return Rule.Get(ca, pos) == '#'; },
                    (List<EChar> ca, int pos) =>
                    {
                        int[] color = new int[6];
                        for (int i = 0; i < 6; i++)
                        {
                            ca.RemoveAt(pos);
                            char c = char.ToLower(Rule.Get(ca, pos));
                            if ((color[i] = colorDefChars.IndexOf(c)) < 0)
                            {
                                TurtleInterpreter.LogError("Invalid color definition!");
                                return;
                            }
                        }
                        ca[pos] = new EChar(Color.FromArgb(color[0] * 16 + color[1], color[2] * 16 + color[3], color[4] * 16 + color[5]));
                    }, 101)
            };
        }

        public static EChar[] Build(List<EChar> ca, bool functionParams = false)
        {
            Rule bestRule;
            int bestPos = 0;
            while (ca.Count > 1)
            {
/*#if DEBUG
                string s = "";
                foreach (EChar ch in ca)
                {
                    s += ch.c;
                }
                TurtleInterpreter.LogError(s);
#endif*/

                bestRule = null;
                for (int i = 0; i < ca.Count; i++)
                {
                    foreach (Rule r in rules)
                    {
                        if ((bestRule == null || r.priority > bestRule.priority) && r.check(ca, i))
                        {
                            bestRule = r;
                            bestPos = i;
                            break;
                        }
                    }
                }
                if (bestRule == null)
                {
                    if (functionParams)
                    {
                        if (ca.Count % 2 == 0)
                        {
                            TurtleInterpreter.LogError("Bad syntax!");
                            return new EChar[] { ca[0] };
                        }
                        List<EChar> l = new List<EChar>();
                        for (int i = 0; i < ca.Count; i++)
                        {
                            if (i % 2 == 0)
                            {
                                if (ca[i].c != 'ⓝ' && ca[i].c != 'ⓒ' && ca[i].c != 'ⓑ')
                                {
                                    TurtleInterpreter.LogError("Bad syntax!");
                                    return new EChar[] { ca[0] };
                                }
                                l.Add(ca[i]);
                            }
                            else
                            {
                                if (ca[i].c != ',')
                                {
                                    TurtleInterpreter.LogError("Bad syntax!");
                                    return new EChar[] { ca[0] };
                                }
                            }
                        }
                        return l.ToArray();
                    }
                    foreach (EChar ec in ca)
                    {
                        if (ec.c != '◯' && ec.c != 'ⓕ' && ec.c != 'ⓛ')
                        {
                            TurtleInterpreter.LogError("Bad syntax!");
                            return new EChar[] { ca[0] };
                        }
                    }
                    break;
                }
                else
                {
                    bestRule.replace(ca, bestPos);
                }
            }
            return ca.ToArray();
        }

        public static ParseTree BuildTree(List<EChar> ca)
        {
            return new ParseTree(new EChar(Build(ca), '◯').simplify());
        }
    }
}
