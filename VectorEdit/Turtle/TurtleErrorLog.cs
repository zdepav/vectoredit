﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VectorEdit.Turtle
{
    class TurtleErrorLog
    {
        private List<string> strs;
        
        public TurtleErrorLog()
        {
            strs = new List<string>();
        }

        public void Add(string s)
        {
            if (s != null) strs.Add(s);
        }

        public int Length { get { return strs.Count; } }

        public override string ToString()
        {
            string s = "";
            for (int i = 0; i < strs.Count; i++)
                s += (i > 0 ? "\n" : "") + strs[i];
            return s;
        }
    }
}
