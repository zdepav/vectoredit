﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VectorEdit.Turtle
{
    class TurtleInterpreter
    {
        private static Regex removeWhitespace = new Regex("\\s+");

        private static Regex validation = new Regex("^([\\+-/\\*%0-9xyacpetfMFBRLCUOSEY\\(\\),:\\{\\}\\.]|(#[0-9a-fA-F]{6}))+$");

        private static TurtleErrorLog Log = null;
        public static Turtle Turtle = null;
        public static List<Line> Lines = null;
        public static Stack<Turtle> Stack = null;

        public static void LogError(string error)
        {
            if (Log != null)
                Log.Add(error);
        }

        public static List<Line> Interpret(string code)
        {
            List<EChar> cs = new List<EChar>();
            code = removeWhitespace.Replace(code, "");
            if (code.Length == 0) return null;
            code = Util.replaceCommands(code);
            if (validation.IsMatch(code))
            {
                foreach (char ch in code)
                    cs.Add(new EChar(ch));
                Log = new TurtleErrorLog();
                ParseTree pt = SyntaxRules.BuildTree(cs);
                if (Log.Length == 0)
                {
                    Lines = new List<Line>();
                    Turtle = new Turtle();
                    Stack = new Stack<Turtle>();
                    pt.Evaluate(Turtle);
                    Turtle = null;
                    Stack = null;
                    List<Line> ls = Lines;
                    Lines = null;
                    if (Log.Length > 0) MessageBox.Show(Log.ToString());
                    Log = null;
                    return ls;
                }
                else MessageBox.Show(Log.ToString());
                Log = null;
                return new List<Line>();
            }
            else MessageBox.Show("Bad syntax ;(", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return null;
        }
    }
}
