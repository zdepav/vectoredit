﻿using System;
using System.Drawing;

namespace VectorEdit.Turtle
{
    class ProcessableValue
    {
        private readonly ParseTree.ValueType type;
        private Color c;
        private float f;
        private bool b;

        public Color SafeGetColor()
        {
            return type == ParseTree.ValueType.Color ? c : Color.Black;
        }

        public float SafeGetNumber(float def = 0)
        { 
            return type == ParseTree.ValueType.Number ? f : def;
        }

        public bool SafeGetBool(bool def = false)
        {
            return type == ParseTree.ValueType.Bool ? b : def;
        }

        public ProcessableValue()
        {
            type = ParseTree.ValueType.NAV;
        }

        public ProcessableValue(Color value)
        {
            type = ParseTree.ValueType.Color;
            c = value;
        }

        public ProcessableValue(float value)
        {
            type = ParseTree.ValueType.Number;
            f = value;
        }

        public ProcessableValue(bool value)
        {
            type = ParseTree.ValueType.Bool;
            b = value;
        }

        public static ProcessableValue operator +(ProcessableValue pv1, ProcessableValue pv2)
        {
            if (pv1.type == ParseTree.ValueType.NAV || pv2.type == ParseTree.ValueType.NAV)
            {
                TurtleInterpreter.LogError("Invalid operation (" + pv1.type + " + " + pv2.type + ")!");
                return new ProcessableValue();
            }
            if (pv1.type == ParseTree.ValueType.Number)
            {
                switch (pv2.type)
                {
                    case ParseTree.ValueType.Number: return new ProcessableValue(pv1.f + pv2.f);
                    case ParseTree.ValueType.Bool: return new ProcessableValue(pv1.f + (pv2.b ? 1 : 0));
                    case ParseTree.ValueType.Color:
                        int i = (int)Math.Round(pv1.f);
                        return new ProcessableValue(Color.FromArgb(
                            Util.Clamp(i + pv2.c.R, 0, 255), Util.Clamp(i + pv2.c.G, 0, 255), Util.Clamp(i + pv2.c.B, 0, 255)));
                }
            }
            else if (pv1.type == ParseTree.ValueType.Bool)
            {
                switch (pv2.type)
                {
                    case ParseTree.ValueType.Number: return new ProcessableValue((pv1.b ? 1 : 0) + pv2.f);
                    case ParseTree.ValueType.Bool: return new ProcessableValue((pv1.b ? 1 : 0) + (pv2.b ? 1 : 0));
                    case ParseTree.ValueType.Color:
                        int i = (pv1.b ? 1 : 0);
                        return new ProcessableValue(Color.FromArgb(
                            Util.Clamp(i + pv2.c.R, 0, 255), Util.Clamp(i + pv2.c.G, 0, 255), Util.Clamp(i + pv2.c.B, 0, 255)));
                }
            }
            else if (pv1.type == ParseTree.ValueType.Color)
            {
                switch (pv2.type)
                {
                    case ParseTree.ValueType.Number:
                        int i = (int)Math.Round(pv2.f);
                        return new ProcessableValue(Color.FromArgb(
                            Util.Clamp(i + pv1.c.R, 0, 255), Util.Clamp(i + pv1.c.G, 0, 255), Util.Clamp(i + pv1.c.B, 0, 255)));
                    case ParseTree.ValueType.Bool:
                        int i2 = (pv2.b ? 1 : 0);
                        return new ProcessableValue(Color.FromArgb(
                            Util.Clamp(i2 + pv1.c.R, 0, 255), Util.Clamp(i2 + pv1.c.G, 0, 255), Util.Clamp(i2 + pv1.c.B, 0, 255)));
                    case ParseTree.ValueType.Color:
                        return new ProcessableValue(Color.FromArgb(
                            Util.Clamp(pv1.c.R + pv2.c.R, 0, 255), Util.Clamp(pv1.c.G + pv2.c.G, 0, 255), Util.Clamp(pv1.c.B + pv2.c.B, 0, 255)));
                }
            }
            return new ProcessableValue();
        }

        public static ProcessableValue operator -(ProcessableValue pv1, ProcessableValue pv2)
        {
            if (pv1.type == ParseTree.ValueType.NAV || pv2.type == ParseTree.ValueType.NAV)
            {
                TurtleInterpreter.LogError("Invalid operation (" + pv1.type + " - " + pv2.type + ")!");
                return new ProcessableValue();
            }
            if (pv1.type == ParseTree.ValueType.Number)
            {
                switch (pv2.type)
                {
                    case ParseTree.ValueType.Number: return new ProcessableValue(pv1.f - pv2.f);
                    case ParseTree.ValueType.Bool: return new ProcessableValue(pv1.f - (pv2.b ? 1 : 0));
                    case ParseTree.ValueType.Color:
                        int i = (int)Math.Round(pv1.f);
                        return new ProcessableValue(Color.FromArgb(
                            Util.Clamp(i - pv2.c.R, 0, 255), Util.Clamp(i - pv2.c.G, 0, 255), Util.Clamp(i - pv2.c.B, 0, 255)));
                }
            }
            else if (pv1.type == ParseTree.ValueType.Bool)
            {
                switch (pv2.type)
                {
                    case ParseTree.ValueType.Number: return new ProcessableValue((pv1.b ? 1 : 0) - pv2.f);
                    case ParseTree.ValueType.Bool: return new ProcessableValue((pv1.b ? 1 : 0) - (pv2.b ? 1 : 0));
                    case ParseTree.ValueType.Color:
                        int i = (pv1.b ? 1 : 0);
                        return new ProcessableValue(Color.FromArgb(
                            Util.Clamp(i - pv2.c.R, 0, 255), Util.Clamp(i - pv2.c.G, 0, 255), Util.Clamp(i - pv2.c.B, 0, 255)));
                }
            }
            else if (pv1.type == ParseTree.ValueType.Color)
            {
                switch (pv2.type)
                {
                    case ParseTree.ValueType.Number:
                        int i = (int)Math.Round(pv2.f);
                        return new ProcessableValue(Color.FromArgb(
                            Util.Clamp(i - pv1.c.R, 0, 255), Util.Clamp(i - pv1.c.G, 0, 255), Util.Clamp(i - pv1.c.B, 0, 255)));
                    case ParseTree.ValueType.Bool:
                        int i2 = (pv2.b ? 1 : 0);
                        return new ProcessableValue(Color.FromArgb(
                            Util.Clamp(i2 - pv1.c.R, 0, 255), Util.Clamp(i2 - pv1.c.G, 0, 255), Util.Clamp(i2 - pv1.c.B, 0, 255)));
                    case ParseTree.ValueType.Color:
                        return new ProcessableValue(Color.FromArgb(
                            Util.Clamp(pv1.c.R - pv2.c.R, 0, 255), Util.Clamp(pv1.c.G - pv2.c.G, 0, 255), Util.Clamp(pv1.c.B - pv2.c.B, 0, 255)));
                }
            }
            return new ProcessableValue();
        }

        public static ProcessableValue operator *(ProcessableValue pv1, ProcessableValue pv2)
        {
            if (pv1.type == ParseTree.ValueType.NAV || pv2.type == ParseTree.ValueType.NAV)
            {
                TurtleInterpreter.LogError("Invalid operation (" + pv1.type + " * " + pv2.type + ")!");
                return new ProcessableValue();
            }
            if (pv1.type == ParseTree.ValueType.Number)
            {
                switch (pv2.type)
                {
                    case ParseTree.ValueType.Number: return new ProcessableValue(pv1.f * pv2.f);
                    case ParseTree.ValueType.Bool: return new ProcessableValue(pv1.f * (pv2.b ? 1 : 0));
                    case ParseTree.ValueType.Color:
                        int i = (int)Math.Round(pv1.f);
                        return new ProcessableValue(Color.FromArgb(
                            Util.Clamp(i * pv2.c.R, 0, 255), Util.Clamp(i * pv2.c.G, 0, 255), Util.Clamp(i * pv2.c.B, 0, 255)));
                }
            }
            else if (pv1.type == ParseTree.ValueType.Bool)
            {
                switch (pv2.type)
                {
                    case ParseTree.ValueType.Number: return new ProcessableValue((pv1.b ? 1 : 0) * pv2.f);
                    case ParseTree.ValueType.Bool: return new ProcessableValue((pv1.b ? 1 : 0) * (pv2.b ? 1 : 0));
                    case ParseTree.ValueType.Color:
                        int i = (pv1.b ? 1 : 0);
                        return new ProcessableValue(Color.FromArgb(
                            Util.Clamp(i * pv2.c.R, 0, 255), Util.Clamp(i * pv2.c.G, 0, 255), Util.Clamp(i * pv2.c.B, 0, 255)));
                }
            }
            else if (pv1.type == ParseTree.ValueType.Color)
            {
                switch (pv2.type)
                {
                    case ParseTree.ValueType.Number:
                        int i = (int)Math.Round(pv2.f);
                        return new ProcessableValue(Color.FromArgb(
                            Util.Clamp(i * pv1.c.R, 0, 255), Util.Clamp(i * pv1.c.G, 0, 255), Util.Clamp(i * pv1.c.B, 0, 255)));
                    case ParseTree.ValueType.Bool:
                        int i2 = (pv2.b ? 1 : 0);
                        return new ProcessableValue(Color.FromArgb(
                            Util.Clamp(i2 * pv1.c.R, 0, 255), Util.Clamp(i2 * pv1.c.G, 0, 255), Util.Clamp(i2 * pv1.c.B, 0, 255)));
                    case ParseTree.ValueType.Color:
                        TurtleInterpreter.LogError("Invalid operation (" + pv1.type + " * " + pv2.type + ")!");
                        return new ProcessableValue();
                }
            }
            return new ProcessableValue();
        }

        public static ProcessableValue operator /(ProcessableValue pv1, ProcessableValue pv2)
        {
            if (pv1.type == ParseTree.ValueType.NAV || pv2.type == ParseTree.ValueType.NAV)
            {
                TurtleInterpreter.LogError("Invalid operation (" + pv1.type + " / " + pv2.type + ")!");
                return new ProcessableValue();
            }
            if (pv1.type == ParseTree.ValueType.Number)
            {
                switch (pv2.type)
                {
                    case ParseTree.ValueType.Number: return new ProcessableValue(pv1.f / pv2.f);
                    case ParseTree.ValueType.Bool:
                        TurtleInterpreter.LogError("Invalid operation (" + pv1.type + " / " + pv2.type + ")!");
                        return new ProcessableValue();
                    case ParseTree.ValueType.Color:
                        TurtleInterpreter.LogError("Invalid operation (" + pv1.type + " / " + pv2.type + ")!");
                        return new ProcessableValue();
                }
            }
            else if (pv1.type == ParseTree.ValueType.Bool)
                TurtleInterpreter.LogError("Invalid operation (" + pv1.type + " / " + pv2.type + ")!");
            else if (pv1.type == ParseTree.ValueType.Color)
            {
                switch (pv2.type)
                {
                    case ParseTree.ValueType.Number:
                        return new ProcessableValue(Color.FromArgb(
                            Util.Clamp((int)Math.Round(pv1.c.R / pv2.f), 0, 255),
                            Util.Clamp((int)Math.Round(pv1.c.G / pv2.f), 0, 255),
                            Util.Clamp((int)Math.Round(pv1.c.B / pv2.f), 0, 255)));
                    default:
                        TurtleInterpreter.LogError("Invalid operation (" + pv1.type + " / " + pv2.type + ")!");
                        break;
                }
            }
            return new ProcessableValue();
        }

        public static ProcessableValue operator %(ProcessableValue pv1, ProcessableValue pv2)
        {
            if (pv1.type == ParseTree.ValueType.NAV || pv2.type == ParseTree.ValueType.NAV)
            {
                TurtleInterpreter.LogError("Invalid operation (" + pv1.type + " % " + pv2.type + ")!");
                return new ProcessableValue();
            }
            if (pv1.type == ParseTree.ValueType.Number)
            {
                switch (pv2.type)
                {
                    case ParseTree.ValueType.Number: return new ProcessableValue(pv1.f % pv2.f);
                    case ParseTree.ValueType.Bool:
                        TurtleInterpreter.LogError("Invalid operation (" + pv1.type + " % " + pv2.type + ")!");
                        return new ProcessableValue();
                    case ParseTree.ValueType.Color:
                        TurtleInterpreter.LogError("Invalid operation (" + pv1.type + " % " + pv2.type + ")!");
                        return new ProcessableValue();
                }
            }
            else if (pv1.type == ParseTree.ValueType.Bool)
                TurtleInterpreter.LogError("Invalid operation (" + pv1.type + " % " + pv2.type + ")!");
            else if (pv1.type == ParseTree.ValueType.Color)
            {
                switch (pv2.type)
                {
                    case ParseTree.ValueType.Number:
                        return new ProcessableValue(Color.FromArgb(
                            Util.Clamp((int)Math.Round(pv1.c.R % pv2.f), 0, 255),
                            Util.Clamp((int)Math.Round(pv1.c.G % pv2.f), 0, 255),
                            Util.Clamp((int)Math.Round(pv1.c.B % pv2.f), 0, 255)));
                    default:
                        TurtleInterpreter.LogError("Invalid operation (" + pv1.type + " % " + pv2.type + ")!");
                        break;
                }
            }
            return new ProcessableValue();
        }
    }
}
