﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VectorEdit.Turtle
{
    class Turtle
    {
        public float x, y, a;

        public Turtle()
        {
            x = y = a = 0;
        }

        public Turtle(Turtle t)
        {
            x = t.x;
            y = t.y;
            a = t.a;
        }

        public void Set(Turtle t)
        {
            x = t.x;
            y = t.y;
            a = t.a;
        }

        public /*bool*/ void Forward(float distance)
        {
            if (distance == 0) return/* false*/;
            double angle = a * Math.PI / 180;
            x += distance * (float)Math.Cos(angle);
            y += distance * (float)Math.Sin(angle);
            //return distance < 0;
        }

        public /*bool*/ void Back(float distance) { /*return */Forward(-distance); }
    }
}
