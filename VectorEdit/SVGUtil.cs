﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;

namespace VectorEdit
{
    internal class SVGUtil
    {
        internal class ReadResult
        {
            public List<string> unrecognizedElements { get; private set; }
            public List<string> errors { get; private set; }
            public List<Shape> shapes { get; private set; }

            public ReadResult()
            {
                unrecognizedElements = new List<string>();
                shapes = new List<Shape>();
                errors = new List<string>();
            }
        }

        public static ReadResult Read(XmlReader xr)
        {
            ReadResult rr = new ReadResult();
            while (rr.errors.Count == 0 && xr.Read())
            {
                if (xr.NodeType == XmlNodeType.Element)
                {
                    switch (xr.Name)
                    {
                        case "svg": break;
                        case "line":
                            rr.shapes.Add(ReadLine(xr, rr));
                            break;
                        case "polyline":
                            rr.shapes.AddRange(ReadPolyline(xr, rr));
                            break;
                        case "rect":
                            rr.shapes.Add(ReadRect(xr, rr));
                            break;
                        case "ellipse":
                            rr.shapes.Add(ReadEllipse(xr, rr));
                            break;
                        case "circle":
                            rr.shapes.Add(ReadCircle(xr, rr));
                            break;
                        default:
                            if (!rr.unrecognizedElements.Contains(xr.Name))
                                rr.unrecognizedElements.Add(xr.Name);
                            break;
                    }
                }
            }
            return rr;
        }

        private static bool TryGetNumericAttribute(string attributeName, XmlReader xr, ReadResult rr, out float value)
        {
            string s = xr.GetAttribute(attributeName);
            if (s != null)
            {
                float buf;
                if (Util.TryParse(s, out buf))
                {
                    value = (int)Math.Round(buf);
                    return true;
                }
                rr.errors.Add(attributeName + " attribute must be a number!");
                value = 0;
                return false;
            }
            else
            {
                rr.errors.Add(attributeName + " attribute must be specified in " + xr.Name + " element!");
                value = 0;
                return false;
            }
        }

        private static SVGStyle GetStyleAttribute(XmlReader xr, ReadResult rr)
        {
            string s = xr.GetAttribute("style");
            if (s != null)
                return new SVGStyle(s, xr);
            return new SVGStyle("", xr);
        }

        private static Line ReadLine(XmlReader xr, ReadResult rr)
        {
            int x, y, x2, y2;
            byte width;
            Color color;
            float buf;
            if (TryGetNumericAttribute("x1", xr, rr, out buf))
                x = (int)Math.Round(buf);
            else return null;
            if (TryGetNumericAttribute("y1", xr, rr, out buf))
                y = (int)Math.Round(buf);
            else return null;
            if (TryGetNumericAttribute("x2", xr, rr, out buf))
                x2 = (int)Math.Round(buf);
            else return null;
            if (TryGetNumericAttribute("y2", xr, rr, out buf))
                y2 = (int)Math.Round(buf);
            else return null;
            SVGStyle svgs = GetStyleAttribute(xr, rr);
            if (!svgs.TryGetColor("stroke", out color))
                color = Color.Black;
            if (svgs.TryGetNumberWithPostfix("stroke-width", out buf, SVGStyle.postFixPX))
                width = (byte)Math.Round(buf);
            else
                width = 1;
            return new Line(x, y, x2, y2, color, width);
        }

        private static Regex pointsRegex = new Regex(@"( [0-9]+(\.[0-9]*)?,[0-9\.]+(\.[0-9]*)?){2,}"),
            singlePoint = new Regex(@"[0-9]+(\.[0-9]*)?,[0-9\.]+(\.[0-9]*)?"),
            whiteSpace = new Regex(@"\s+"), comma = new Regex(@" ?, ?");

        private static Line[] ReadPolyline(XmlReader xr, ReadResult rr)
        {
            List<Point> points = new List<Point>();
            byte width;
            Color color;
            float buf;
            List<Line> result = new List<Line>();
            string s = xr.GetAttribute("points");
            if (s == null)
            {
                rr.errors.Add("polyline element must contain points attribute!");
                return null;
            }
            s = s.Trim();
            s = whiteSpace.Replace(s, " ");
            s = comma.Replace(s, ",");
            if (!pointsRegex.IsMatch(' ' + s))
            {
                rr.errors.Add("polyline element must contain valid points attribute!");
                return null;
            }
            MatchCollection mc = singlePoint.Matches(s);
            foreach (Match m in mc)
            {
                float b1, b2;
                string[] ss = m.Value.Split(',');
                if (Util.TryParse(ss[0], out b1) && Util.TryParse(ss[1], out b2))
                {
                    points.Add(new Point((int)Math.Round(b1), (int)Math.Round(b2)));
                }
                else
                {
                    rr.errors.Add("polyline element must contain valid points attribute!");
                    return null;
                }
            }
            SVGStyle svgs = GetStyleAttribute(xr, rr);
            if (!svgs.TryGetColor("stroke", out color))
                color = Color.Black;
            if (svgs.TryGetNumberWithPostfix("stroke-width", out buf, SVGStyle.postFixPX))
                width = (byte)Math.Round(buf);
            else
                width = 1;
            for (int i = 1; i < points.Count; i++)
            {
                result.Add(new Line(points[i - 1].X, points[i - 1].Y, points[i].X, points[i].Y, color, width));
            }
            return result.ToArray();
        }

        private static Rect ReadRect(XmlReader xr, ReadResult rr)
        {
            int x, y, w, h;
            byte width;
            Color color;
            float buf;
            if (TryGetNumericAttribute("x", xr, rr, out buf))
                x = (int)Math.Round(buf);
            else x = 0;
            if (TryGetNumericAttribute("y", xr, rr, out buf))
                y = (int)Math.Round(buf);
            else y = 0;
            if (TryGetNumericAttribute("width", xr, rr, out buf))
                w = (int)Math.Round(buf);
            else return null;
            if (TryGetNumericAttribute("height", xr, rr, out buf))
                h = (int)Math.Round(buf);
            else return null;
            SVGStyle svgs = GetStyleAttribute(xr, rr);
            if (!svgs.TryGetColor("stroke", out color))
                color = Color.Black;
            if (svgs.TryGetNumberWithPostfix("stroke-width", out buf, SVGStyle.postFixPX))
                width = (byte)Math.Round(buf);
            else
                width = 1;
            Rect r = new Rect(x, y, x + w, y + h, color, width);
            FillColor(svgs, r);
            return r;
        }

        private static void FillColor(SVGStyle svgs, Rect r)
        {
            float buf, buf2;
            Color color;
            if (svgs.TryGetColor("fill", out color, true))
            {
                if (svgs.TryGetNumber("fill-opacity", out buf))
                {
                    if (svgs.TryGetNumber("opacity", out buf2))
                    {
                        r.fillColor = Color.FromArgb((int)Math.Round(buf * buf2 * color.A), color.R, color.G, color.B);
                    }
                    else r.fillColor = Color.FromArgb((int)Math.Round(buf * color.A), color.R, color.G, color.B);
                }
                else r.fillColor = color;
            }
            else r.fillColor = Color.Black;
        }

        private static void FillColor(SVGStyle svgs, Ellipse e)
        {
            float buf, buf2;
            Color color;
            if (svgs.TryGetColor("fill", out color, true))
            {
                if (svgs.TryGetNumber("fill-opacity", out buf))
                {
                    if (svgs.TryGetNumber("opacity", out buf2))
                    {
                        e.fillColor = Color.FromArgb((int)Math.Round(buf * buf2 * color.A), color.R, color.G, color.B);
                    }
                    else e.fillColor = Color.FromArgb((int)Math.Round(buf * color.A), color.R, color.G, color.B);
                }
                else e.fillColor = color;
            }
            else e.fillColor = Color.Black;
        }

        private static Ellipse ReadEllipse(XmlReader xr, ReadResult rr)
        {
            int cx, cy, a, b;
            byte width;
            Color color;
            float buf;
            if (TryGetNumericAttribute("cx", xr, rr, out buf))
                cx = (int)Math.Round(buf);
            else cx = 0;
            if (TryGetNumericAttribute("cy", xr, rr, out buf))
                cy = (int)Math.Round(buf);
            else cy = 0;
            if (TryGetNumericAttribute("rx", xr, rr, out buf))
                a = (int)Math.Round(buf);
            else return null;
            if (TryGetNumericAttribute("ry", xr, rr, out buf))
                b = (int)Math.Round(buf);
            else return null;
            SVGStyle svgs = GetStyleAttribute(xr, rr);
            if (!svgs.TryGetColor("stroke", out color))
                color = Color.Black;
            if (svgs.TryGetNumberWithPostfix("stroke-width", out buf, SVGStyle.postFixPX))
                width = (byte)Math.Round(buf);
            else
                width = 1;
            Ellipse e = new Ellipse(cx, cy, cx + a, cy + b, color, width);
            FillColor(svgs, e);
            return e;
        }

        private static Ellipse ReadCircle(XmlReader xr, ReadResult rr)
        {
            int cx, cy, r;
            byte width;
            Color color;
            float buf;
            if (TryGetNumericAttribute("cx", xr, rr, out buf))
                cx = (int)Math.Round(buf);
            else cx = 0;
            if (TryGetNumericAttribute("cy", xr, rr, out buf))
                cy = (int)Math.Round(buf);
            else cy = 0;
            if (TryGetNumericAttribute("r", xr, rr, out buf))
                r = (int)Math.Round(buf);
            else return null;
            SVGStyle svgs = GetStyleAttribute(xr, rr);
            if (!svgs.TryGetColor("stroke", out color))
                color = Color.Black;
            if (svgs.TryGetNumberWithPostfix("stroke-width", out buf, SVGStyle.postFixPX))
                width = (byte)Math.Round(buf);
            else
                width = 1;
            Ellipse e = new Ellipse(cx, cy, cx + r, cy + r, color, width);
            FillColor(svgs, e);
            return e;
        }

        internal class SVGStyle
        {
            private Dictionary<string, string> styles;

            public SVGStyle(string styleString)
            {
                styles = new Dictionary<string, string>();
                if (styleString.Length > 0)
                {
                    string buf = "", name = "";
                    bool phase1 = true;
                    int x = 1;
                    for (int i = 0; i < styleString.Length; i++)
                    {
                        if (phase1)
                        {
                            if (char.IsWhiteSpace(styleString, i))
                            {
                                if (buf.Length > 0) buf = "";
                                continue;
                            }
                            if (styleString[i] == ':')
                            {
                                if (buf.Length > 0)
                                {
                                    name = buf;
                                    buf = "";
                                }
                                else
                                {
                                    name = "___unknown_" + x + "___";
                                    x++;
                                }
                                phase1 = false;
                            }
                            else buf += styleString[i];
                        }
                        else
                        {
                            if (styleString[i] == ';')
                            {
                                if (buf.Length > 0)
                                {
                                    styles.Add(name, buf.Trim());
                                    buf = "";
                                }
                                phase1 = true;
                            }
                            else buf += styleString[i];
                        }
                    }
                    if (!phase1 && buf.Length > 0)
                    {
                        styles.Add(name, buf.Trim());
                        buf = "";
                    }
                }
            }

            public SVGStyle(string styleString, XmlReader xr)
                : this(styleString)
            {
                AddFromAttributes(xr);
            }

            public void AddFromAttributes(XmlReader xr)
            {
                for (int i = 0; i < xr.AttributeCount; i++)
                {
                    xr.MoveToAttribute(i);
                    if (!styles.ContainsKey(xr.Name))
                        styles.Add(xr.Name, xr.Value);
                }
                xr.MoveToElement();
            }

            public bool TryGetNumber(string identifier, out float value)
            {
                string s;
                if (styles.TryGetValue(identifier, out s))
                    return Util.TryParse(s, out value);
                else
                {
                    value = 0;
                    return false;
                }
            }

            public bool TryGet(string identifier, out string value)
            {
                return styles.TryGetValue(identifier, out value);
            }

            public static Regex postFixPX = new Regex("px$");

            public bool TryGetNumberWithPostfix(string identifier, out float value, Regex postFix)
            {
                string s;
                if (styles.TryGetValue(identifier, out s))
                    return Util.TryParse(postFix.Replace(s, ""), out value);
                else
                {
                    value = 0;
                    return false;
                }
            }

            private static Regex colorHex = new Regex("^#[0-9a-fA-F]{6}$"), colorName = new Regex("^[a-zA-Z]+$");

            private static string hexstr = "0123456789abcdef";

            public bool TryGetColor(string identifier, out Color value, bool withAlpha = false)
            {
                value = Color.Black;
                string s;
                if (styles.TryGetValue(identifier, out s))
                {
                    if (colorName.IsMatch(s))
                    {
                        Color c = Color.FromName(s);
                        if (!withAlpha && c.A < 255)
                            value = Color.FromArgb(c.R, c.G, c.B);
                        else value = c;
                        return true;
                    }
                    else if (colorHex.IsMatch(s))
                    {
                        s = s.ToLower();
                        value = Color.FromArgb(
                            hexstr.IndexOf(s[1]) * 16 + hexstr.IndexOf(s[2]),
                            hexstr.IndexOf(s[3]) * 16 + hexstr.IndexOf(s[4]),
                            hexstr.IndexOf(s[5]) * 16 + hexstr.IndexOf(s[6]));
                        return true;
                    }
                    else
                    {
                        List<float> floats = new List<float>();
                        if (s.StartsWith("rgb") && ReadParamFloatsFromString(s.Substring(3), floats, "(_,_,_)", new NumberMinMax(0, 255), new NumberMinMax(0, 255), new NumberMinMax(0, 255)))
                        {
                            value = Color.FromArgb((int)Math.Round(floats[0]), (int)Math.Round(floats[1]), (int)Math.Round(floats[2]));
                            return true;
                        }
                        else if (s.StartsWith("rgba") && ReadParamFloatsFromString(s.Substring(4), floats, "(_,_,_,_)", new NumberMinMax(0, 255), new NumberMinMax(0, 255), new NumberMinMax(0, 255), new NumberMinMax(0, 1)))
                        {
                            value = Color.FromArgb((int)Math.Round(floats[3] * 255), (int)Math.Round(floats[0]), (int)Math.Round(floats[1]), (int)Math.Round(floats[2]));
                            if (!withAlpha && value.A < 255)
                                value = Color.FromArgb(value.R, value.G, value.B);
                            return true;
                        }
                        else if (s.StartsWith("hsl") && ReadParamFloatsFromString(s.Substring(3), floats, "(_,_%,_%)", new NumberMinMax(0, 360), new NumberMinMax(0, 100), new NumberMinMax(0, 100)))
                        {
                            value = Util.RGBAFromHSL(Util.Clamp(floats[0], 0, 360), Util.Clamp(floats[1], 0, 100), Util.Clamp(floats[2], 0, 100));
                            return true;
                        }
                        else if (s.StartsWith("hsla") && ReadParamFloatsFromString(s.Substring(4), floats, "(_,_%,_%,_)", new NumberMinMax(0, 360), new NumberMinMax(0, 100), new NumberMinMax(0, 100), new NumberMinMax(0, 1)))
                        {
                            value = Util.RGBAFromHSLA(Util.Clamp(floats[0], 0, 360), Util.Clamp(floats[1], 0, 100), Util.Clamp(floats[2], 0, 100), floats[3]);
                            if (!withAlpha && value.A < 255)
                                value = Color.FromArgb(value.R, value.G, value.B);
                            return true;
                        }
                    }
                }
                return false;
            }

            public static bool ReadParamFloatsFromString(string s, List<float> floats, string format, params NumberMinMax[] minmaxs)
            {
                floats.Clear();
                int count = 0; foreach (char ch in format) { if (ch == '_') count++; }
                if (count != minmaxs.Length)
                    return false;
                int c = 0;
                string numBuf;
                int i, j = 0;
                float f;
                bool b;
                for (i = 0; i < s.Length; i++)
                {
                    if (char.IsWhiteSpace(s, i))
                        continue;
                    else
                    {
                        if (j >= format.Length)
                        {
                            floats.Clear();
                            return false;
                        }
                        if (format[j] == '_')
                        {
                            if (char.IsDigit(s, i))
                            {
                                numBuf = "";
                                b = true;
                                while (i < s.Length)
                                {
                                    if (char.IsDigit(s, i))
                                        numBuf += s[i];
                                    else if (b && s[i] == '.')
                                    {
                                        numBuf += s[i];
                                        b = false;
                                    }
                                    else break;
                                    i++;
                                }
                                i--;
                                j++;
                                b = true;
                                if (Util.TryParse(numBuf, out f))
                                {
                                    if (minmaxs[c].IsMatch(f))
                                    {
                                        floats.Add(f);
                                        c++;
                                    }
                                    else b = false;
                                }
                                else b = false;
                                if (!b)
                                {
                                    floats.Clear();
                                    return false;
                                }
                                continue;
                            }
                        }
                        else if (s[i] == format[j])
                        {
                            j++;
                            continue;
                        }
                        else
                        {
                            floats.Clear();
                            return false;
                        }
                    }
                }
                if (j == format.Length && floats.Count == count)
                    return true;
                floats.Clear();
                return false;
            }

            public struct NumberMinMax
            {
                public float min, max;
                public NumberMinMax(float min, float max)
                {
                    this.min = min;
                    this.max = max;
                }
                public bool IsMatch(float f)
                {
                    return (min < f || Math.Abs(min - f) < 1000 * float.Epsilon) && (max > f || Math.Abs(max - f) < 1000 * float.Epsilon);
                }
            }
        }
    }
}