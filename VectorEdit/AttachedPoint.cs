﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VectorEdit
{
    internal class AttachedPoint
    {
        public Point p { get; private set; }
        public Image i { get; private set; }

        public AttachedPoint(Point point, Image image)
        {
            p = point;
            i = image;
        }
    }
}
