﻿namespace VectorEdit
{
    partial class ColorPicker
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.inputB = new System.Windows.Forms.NumericUpDown();
            this.inputG = new System.Windows.Forms.NumericUpDown();
            this.inputR = new System.Windows.Forms.NumericUpDown();
            this.inputH = new System.Windows.Forms.NumericUpDown();
            this.inputS = new System.Windows.Forms.NumericUpDown();
            this.inputL = new System.Windows.Forms.NumericUpDown();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.inputA = new System.Windows.Forms.NumericUpDown();
            this.labelA = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.inputB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.inputG)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.inputR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.inputH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.inputS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.inputL)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.inputA)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(7, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(22, 19);
            this.label1.TabIndex = 0;
            this.label1.Text = "R:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(87, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(23, 19);
            this.label2.TabIndex = 1;
            this.label2.Text = "G:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(170, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(22, 19);
            this.label3.TabIndex = 2;
            this.label3.Text = "B:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.Location = new System.Drawing.Point(7, 25);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(23, 19);
            this.label4.TabIndex = 3;
            this.label4.Text = "H:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label5.Location = new System.Drawing.Point(90, 25);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(20, 19);
            this.label5.TabIndex = 4;
            this.label5.Text = "S:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label6.Location = new System.Drawing.Point(170, 25);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(20, 19);
            this.label6.TabIndex = 5;
            this.label6.Text = "L:";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(12, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(256, 256);
            this.pictureBox1.TabIndex = 6;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox1_Paint);
            this.pictureBox1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseDown);
            this.pictureBox1.MouseLeave += new System.EventHandler(this.pictureBox1_MouseLeave);
            this.pictureBox1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseMove);
            this.pictureBox1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseUp);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button1.Location = new System.Drawing.Point(0, 53);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(96, 24);
            this.button1.TabIndex = 13;
            this.button1.Text = "OK";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button2.Location = new System.Drawing.Point(160, 53);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(96, 24);
            this.button2.TabIndex = 14;
            this.button2.Text = "Cancel";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Location = new System.Drawing.Point(12, 274);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(256, 24);
            this.pictureBox2.TabIndex = 15;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox2_Paint);
            this.pictureBox2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox2_MouseDown);
            this.pictureBox2.MouseLeave += new System.EventHandler(this.pictureBox2_MouseLeave);
            this.pictureBox2.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBox2_MouseMove);
            this.pictureBox2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox2_MouseUp);
            // 
            // inputB
            // 
            this.inputB.Location = new System.Drawing.Point(196, 1);
            this.inputB.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.inputB.Name = "inputB";
            this.inputB.Size = new System.Drawing.Size(48, 20);
            this.inputB.TabIndex = 21;
            this.inputB.ValueChanged += new System.EventHandler(this.inputRGB_ValueChanged);
            // 
            // inputG
            // 
            this.inputG.Location = new System.Drawing.Point(116, 1);
            this.inputG.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.inputG.Name = "inputG";
            this.inputG.Size = new System.Drawing.Size(48, 20);
            this.inputG.TabIndex = 22;
            this.inputG.ValueChanged += new System.EventHandler(this.inputRGB_ValueChanged);
            // 
            // inputR
            // 
            this.inputR.Location = new System.Drawing.Point(36, 1);
            this.inputR.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.inputR.Name = "inputR";
            this.inputR.Size = new System.Drawing.Size(48, 20);
            this.inputR.TabIndex = 23;
            this.inputR.ValueChanged += new System.EventHandler(this.inputRGB_ValueChanged);
            // 
            // inputH
            // 
            this.inputH.Location = new System.Drawing.Point(36, 27);
            this.inputH.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
            this.inputH.Name = "inputH";
            this.inputH.Size = new System.Drawing.Size(48, 20);
            this.inputH.TabIndex = 24;
            this.inputH.ValueChanged += new System.EventHandler(this.inputHSL_ValueChanged);
            // 
            // inputS
            // 
            this.inputS.Location = new System.Drawing.Point(116, 27);
            this.inputS.Name = "inputS";
            this.inputS.Size = new System.Drawing.Size(48, 20);
            this.inputS.TabIndex = 25;
            this.inputS.ValueChanged += new System.EventHandler(this.inputHSL_ValueChanged);
            // 
            // inputL
            // 
            this.inputL.Location = new System.Drawing.Point(196, 27);
            this.inputL.Name = "inputL";
            this.inputL.Size = new System.Drawing.Size(48, 20);
            this.inputL.TabIndex = 26;
            this.inputL.ValueChanged += new System.EventHandler(this.inputHSL_ValueChanged);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.pictureBox4);
            this.panel1.Controls.Add(this.inputR);
            this.panel1.Controls.Add(this.inputL);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.inputS);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.inputH);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.inputG);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.inputB);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.button2);
            this.panel1.Location = new System.Drawing.Point(12, 334);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(256, 77);
            this.panel1.TabIndex = 27;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Location = new System.Drawing.Point(102, 53);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(52, 24);
            this.pictureBox4.TabIndex = 29;
            this.pictureBox4.TabStop = false;
            this.pictureBox4.Click += new System.EventHandler(this.pictureBox4_Click);
            this.pictureBox4.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox4_Paint);
            // 
            // pictureBox3
            // 
            this.pictureBox3.Location = new System.Drawing.Point(92, 304);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(176, 24);
            this.pictureBox3.TabIndex = 28;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox3_Paint);
            // 
            // inputA
            // 
            this.inputA.Location = new System.Drawing.Point(38, 306);
            this.inputA.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.inputA.Name = "inputA";
            this.inputA.Size = new System.Drawing.Size(48, 20);
            this.inputA.TabIndex = 27;
            this.inputA.ValueChanged += new System.EventHandler(this.inputA_ValueChanged);
            // 
            // labelA
            // 
            this.labelA.AutoSize = true;
            this.labelA.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelA.Location = new System.Drawing.Point(12, 306);
            this.labelA.Name = "labelA";
            this.labelA.Size = new System.Drawing.Size(22, 19);
            this.labelA.TabIndex = 27;
            this.labelA.Text = "A:";
            // 
            // ColorPicker
            // 
            this.AcceptButton = this.button1;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.button2;
            this.ClientSize = new System.Drawing.Size(280, 423);
            this.Controls.Add(this.labelA);
            this.Controls.Add(this.inputA);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ColorPicker";
            this.Text = "ColorPicker";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.inputB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.inputG)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.inputR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.inputH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.inputS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.inputL)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.inputA)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.NumericUpDown inputB;
        private System.Windows.Forms.NumericUpDown inputG;
        private System.Windows.Forms.NumericUpDown inputR;
        private System.Windows.Forms.NumericUpDown inputH;
        private System.Windows.Forms.NumericUpDown inputS;
        private System.Windows.Forms.NumericUpDown inputL;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.NumericUpDown inputA;
        private System.Windows.Forms.Label labelA;
        private System.Windows.Forms.PictureBox pictureBox4;
    }
}