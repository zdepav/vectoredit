﻿namespace VectorEdit
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.pbLine = new System.Windows.Forms.PictureBox();
            this.pbRect = new System.Windows.Forms.PictureBox();
            this.pbCirc = new System.Windows.Forms.PictureBox();
            this.groupShapes = new System.Windows.Forms.GroupBox();
            this.labelCirc = new System.Windows.Forms.Label();
            this.labelRect = new System.Windows.Forms.Label();
            this.labelLine = new System.Windows.Forms.Label();
            this.groupActions = new System.Windows.Forms.GroupBox();
            this.labelMovC = new System.Windows.Forms.Label();
            this.labelMove = new System.Windows.Forms.Label();
            this.labelEdit = new System.Windows.Forms.Label();
            this.pbEdit = new System.Windows.Forms.PictureBox();
            this.labelDraw = new System.Windows.Forms.Label();
            this.pbMove = new System.Windows.Forms.PictureBox();
            this.pbDraw = new System.Windows.Forms.PictureBox();
            this.pbMovC = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.checkBoxGrid = new System.Windows.Forms.CheckBox();
            this.pbGrid = new System.Windows.Forms.PictureBox();
            this.pbColor = new System.Windows.Forms.PictureBox();
            this.buttonColor = new System.Windows.Forms.Button();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.selectAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deselectAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resetCanvasPositionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.turtleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbLine)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbRect)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCirc)).BeginInit();
            this.groupShapes.SuspendLayout();
            this.groupActions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbMove)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbDraw)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbMovC)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbColor)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.Location = new System.Drawing.Point(12, 36);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(800, 600);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            this.pictureBox1.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox1_Paint);
            this.pictureBox1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseClick);
            this.pictureBox1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseDown);
            this.pictureBox1.MouseLeave += new System.EventHandler(this.pictureBox1_MouseLeave);
            this.pictureBox1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseMove);
            this.pictureBox1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseUp);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 18;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // pbLine
            // 
            this.pbLine.Location = new System.Drawing.Point(6, 19);
            this.pbLine.Name = "pbLine";
            this.pbLine.Size = new System.Drawing.Size(24, 24);
            this.pbLine.TabIndex = 4;
            this.pbLine.TabStop = false;
            this.pbLine.Click += new System.EventHandler(this.SelectToolLine);
            this.pbLine.Paint += new System.Windows.Forms.PaintEventHandler(this.pb_Paint);
            // 
            // pbRect
            // 
            this.pbRect.Location = new System.Drawing.Point(6, 44);
            this.pbRect.Name = "pbRect";
            this.pbRect.Size = new System.Drawing.Size(24, 24);
            this.pbRect.TabIndex = 8;
            this.pbRect.TabStop = false;
            this.pbRect.Click += new System.EventHandler(this.SelectToolRectangle);
            this.pbRect.Paint += new System.Windows.Forms.PaintEventHandler(this.pb_Paint);
            // 
            // pbCirc
            // 
            this.pbCirc.Location = new System.Drawing.Point(6, 69);
            this.pbCirc.Name = "pbCirc";
            this.pbCirc.Size = new System.Drawing.Size(24, 24);
            this.pbCirc.TabIndex = 9;
            this.pbCirc.TabStop = false;
            this.pbCirc.Click += new System.EventHandler(this.SelectToolEllipse);
            this.pbCirc.Paint += new System.Windows.Forms.PaintEventHandler(this.pb_Paint);
            // 
            // groupShapes
            // 
            this.groupShapes.Controls.Add(this.labelCirc);
            this.groupShapes.Controls.Add(this.labelRect);
            this.groupShapes.Controls.Add(this.labelLine);
            this.groupShapes.Controls.Add(this.pbRect);
            this.groupShapes.Controls.Add(this.pbLine);
            this.groupShapes.Controls.Add(this.pbCirc);
            this.groupShapes.Location = new System.Drawing.Point(818, 36);
            this.groupShapes.Name = "groupShapes";
            this.groupShapes.Size = new System.Drawing.Size(142, 99);
            this.groupShapes.TabIndex = 11;
            this.groupShapes.TabStop = false;
            this.groupShapes.Text = "Shapes";
            // 
            // labelCirc
            // 
            this.labelCirc.AutoSize = true;
            this.labelCirc.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelCirc.Location = new System.Drawing.Point(36, 72);
            this.labelCirc.Name = "labelCirc";
            this.labelCirc.Size = new System.Drawing.Size(49, 18);
            this.labelCirc.TabIndex = 12;
            this.labelCirc.Text = "Ellipse";
            this.labelCirc.Click += new System.EventHandler(this.SelectToolEllipse);
            // 
            // labelRect
            // 
            this.labelRect.AutoSize = true;
            this.labelRect.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelRect.Location = new System.Drawing.Point(36, 47);
            this.labelRect.Name = "labelRect";
            this.labelRect.Size = new System.Drawing.Size(69, 18);
            this.labelRect.TabIndex = 11;
            this.labelRect.Text = "Rectangle";
            this.labelRect.Click += new System.EventHandler(this.SelectToolRectangle);
            // 
            // labelLine
            // 
            this.labelLine.AutoSize = true;
            this.labelLine.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelLine.Location = new System.Drawing.Point(36, 22);
            this.labelLine.Name = "labelLine";
            this.labelLine.Size = new System.Drawing.Size(34, 18);
            this.labelLine.TabIndex = 10;
            this.labelLine.Text = "Line";
            this.labelLine.Click += new System.EventHandler(this.SelectToolLine);
            // 
            // groupActions
            // 
            this.groupActions.Controls.Add(this.labelMovC);
            this.groupActions.Controls.Add(this.labelMove);
            this.groupActions.Controls.Add(this.labelEdit);
            this.groupActions.Controls.Add(this.pbEdit);
            this.groupActions.Controls.Add(this.labelDraw);
            this.groupActions.Controls.Add(this.pbMove);
            this.groupActions.Controls.Add(this.pbDraw);
            this.groupActions.Controls.Add(this.pbMovC);
            this.groupActions.Location = new System.Drawing.Point(818, 141);
            this.groupActions.Name = "groupActions";
            this.groupActions.Size = new System.Drawing.Size(142, 125);
            this.groupActions.TabIndex = 13;
            this.groupActions.TabStop = false;
            this.groupActions.Text = "Actions";
            // 
            // labelMovC
            // 
            this.labelMovC.AutoSize = true;
            this.labelMovC.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelMovC.Location = new System.Drawing.Point(36, 97);
            this.labelMovC.Name = "labelMovC";
            this.labelMovC.Size = new System.Drawing.Size(89, 18);
            this.labelMovC.TabIndex = 19;
            this.labelMovC.Text = "Move Canvas";
            this.labelMovC.Click += new System.EventHandler(this.SelectActionMoveCanvas);
            // 
            // labelMove
            // 
            this.labelMove.AutoSize = true;
            this.labelMove.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelMove.Location = new System.Drawing.Point(36, 72);
            this.labelMove.Name = "labelMove";
            this.labelMove.Size = new System.Drawing.Size(43, 18);
            this.labelMove.TabIndex = 18;
            this.labelMove.Text = "Move";
            this.labelMove.Click += new System.EventHandler(this.SelectActionMove);
            // 
            // labelEdit
            // 
            this.labelEdit.AutoSize = true;
            this.labelEdit.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelEdit.Location = new System.Drawing.Point(36, 47);
            this.labelEdit.Name = "labelEdit";
            this.labelEdit.Size = new System.Drawing.Size(32, 18);
            this.labelEdit.TabIndex = 14;
            this.labelEdit.Text = "Edit";
            this.labelEdit.Click += new System.EventHandler(this.SelectActionEdit);
            // 
            // pbEdit
            // 
            this.pbEdit.Location = new System.Drawing.Point(6, 44);
            this.pbEdit.Name = "pbEdit";
            this.pbEdit.Size = new System.Drawing.Size(24, 24);
            this.pbEdit.TabIndex = 17;
            this.pbEdit.TabStop = false;
            this.pbEdit.Click += new System.EventHandler(this.SelectActionEdit);
            this.pbEdit.Paint += new System.Windows.Forms.PaintEventHandler(this.pb_Paint);
            // 
            // labelDraw
            // 
            this.labelDraw.AutoSize = true;
            this.labelDraw.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelDraw.Location = new System.Drawing.Point(36, 22);
            this.labelDraw.Name = "labelDraw";
            this.labelDraw.Size = new System.Drawing.Size(40, 18);
            this.labelDraw.TabIndex = 13;
            this.labelDraw.Text = "Draw";
            this.labelDraw.Click += new System.EventHandler(this.SelectActionDraw);
            // 
            // pbMove
            // 
            this.pbMove.Location = new System.Drawing.Point(6, 69);
            this.pbMove.Name = "pbMove";
            this.pbMove.Size = new System.Drawing.Size(24, 24);
            this.pbMove.TabIndex = 14;
            this.pbMove.TabStop = false;
            this.pbMove.Click += new System.EventHandler(this.SelectActionMove);
            this.pbMove.Paint += new System.Windows.Forms.PaintEventHandler(this.pb_Paint);
            // 
            // pbDraw
            // 
            this.pbDraw.Location = new System.Drawing.Point(6, 19);
            this.pbDraw.Name = "pbDraw";
            this.pbDraw.Size = new System.Drawing.Size(24, 24);
            this.pbDraw.TabIndex = 13;
            this.pbDraw.TabStop = false;
            this.pbDraw.Click += new System.EventHandler(this.SelectActionDraw);
            this.pbDraw.Paint += new System.Windows.Forms.PaintEventHandler(this.pb_Paint);
            // 
            // pbMovC
            // 
            this.pbMovC.Location = new System.Drawing.Point(6, 94);
            this.pbMovC.Name = "pbMovC";
            this.pbMovC.Size = new System.Drawing.Size(24, 24);
            this.pbMovC.TabIndex = 15;
            this.pbMovC.TabStop = false;
            this.pbMovC.Click += new System.EventHandler(this.SelectActionMoveCanvas);
            this.pbMovC.Paint += new System.Windows.Forms.PaintEventHandler(this.pb_Paint);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.checkBoxGrid);
            this.groupBox1.Controls.Add(this.pbGrid);
            this.groupBox1.Controls.Add(this.pbColor);
            this.groupBox1.Controls.Add(this.buttonColor);
            this.groupBox1.Location = new System.Drawing.Point(818, 272);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(142, 364);
            this.groupBox1.TabIndex = 18;
            this.groupBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 75);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 13);
            this.label1.TabIndex = 23;
            // 
            // checkBoxGrid
            // 
            this.checkBoxGrid.AutoSize = true;
            this.checkBoxGrid.Location = new System.Drawing.Point(37, 48);
            this.checkBoxGrid.Name = "checkBoxGrid";
            this.checkBoxGrid.Size = new System.Drawing.Size(75, 17);
            this.checkBoxGrid.TabIndex = 22;
            this.checkBoxGrid.Text = "Show Grid";
            this.checkBoxGrid.UseVisualStyleBackColor = true;
            this.checkBoxGrid.CheckedChanged += new System.EventHandler(this.checkBoxGrid_CheckedChanged);
            // 
            // pbGrid
            // 
            this.pbGrid.Location = new System.Drawing.Point(6, 44);
            this.pbGrid.Name = "pbGrid";
            this.pbGrid.Size = new System.Drawing.Size(24, 24);
            this.pbGrid.TabIndex = 21;
            this.pbGrid.TabStop = false;
            this.pbGrid.Paint += new System.Windows.Forms.PaintEventHandler(this.pbGrid_Paint);
            // 
            // pbColor
            // 
            this.pbColor.Location = new System.Drawing.Point(6, 19);
            this.pbColor.Name = "pbColor";
            this.pbColor.Size = new System.Drawing.Size(24, 24);
            this.pbColor.TabIndex = 20;
            this.pbColor.TabStop = false;
            this.pbColor.Paint += new System.Windows.Forms.PaintEventHandler(this.pbColor_Paint);
            // 
            // buttonColor
            // 
            this.buttonColor.Location = new System.Drawing.Point(36, 19);
            this.buttonColor.Name = "buttonColor";
            this.buttonColor.Size = new System.Drawing.Size(100, 24);
            this.buttonColor.TabIndex = 19;
            this.buttonColor.Text = "Choose Color";
            this.buttonColor.UseVisualStyleBackColor = true;
            this.buttonColor.Click += new System.EventHandler(this.buttonColor_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.Title = "Open file";
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.White;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.toolsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(972, 24);
            this.menuStrip1.TabIndex = 19;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.openToolStripMenuItem,
            this.exportToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.Size = new System.Drawing.Size(143, 22);
            this.newToolStripMenuItem.Text = "New";
            this.newToolStripMenuItem.Click += new System.EventHandler(this.newToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(143, 22);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.L)));
            this.openToolStripMenuItem.Size = new System.Drawing.Size(143, 22);
            this.openToolStripMenuItem.Text = "Open";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // exportToolStripMenuItem
            // 
            this.exportToolStripMenuItem.Name = "exportToolStripMenuItem";
            this.exportToolStripMenuItem.Size = new System.Drawing.Size(143, 22);
            this.exportToolStripMenuItem.Text = "Export";
            this.exportToolStripMenuItem.Click += new System.EventHandler(this.exportToolStripMenuItem_Click);
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.selectAllToolStripMenuItem,
            this.deselectAllToolStripMenuItem,
            this.resetCanvasPositionToolStripMenuItem,
            this.turtleToolStripMenuItem});
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            this.toolsToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.toolsToolStripMenuItem.Text = "Tools";
            // 
            // selectAllToolStripMenuItem
            // 
            this.selectAllToolStripMenuItem.Name = "selectAllToolStripMenuItem";
            this.selectAllToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.A)));
            this.selectAllToolStripMenuItem.Size = new System.Drawing.Size(230, 22);
            this.selectAllToolStripMenuItem.Text = "SelectAll";
            this.selectAllToolStripMenuItem.Click += new System.EventHandler(this.selectAllToolStripMenuItem_Click);
            // 
            // deselectAllToolStripMenuItem
            // 
            this.deselectAllToolStripMenuItem.Name = "deselectAllToolStripMenuItem";
            this.deselectAllToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D)));
            this.deselectAllToolStripMenuItem.Size = new System.Drawing.Size(230, 22);
            this.deselectAllToolStripMenuItem.Text = "Deselect All";
            this.deselectAllToolStripMenuItem.Click += new System.EventHandler(this.deselectAllToolStripMenuItem_Click);
            // 
            // resetCanvasPositionToolStripMenuItem
            // 
            this.resetCanvasPositionToolStripMenuItem.Name = "resetCanvasPositionToolStripMenuItem";
            this.resetCanvasPositionToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.R)));
            this.resetCanvasPositionToolStripMenuItem.Size = new System.Drawing.Size(230, 22);
            this.resetCanvasPositionToolStripMenuItem.Text = "Reset Canvas Position";
            this.resetCanvasPositionToolStripMenuItem.Click += new System.EventHandler(this.resetCanvasPositionToolStripMenuItem_Click);
            // 
            // turtleToolStripMenuItem
            // 
            this.turtleToolStripMenuItem.Name = "turtleToolStripMenuItem";
            this.turtleToolStripMenuItem.Size = new System.Drawing.Size(230, 22);
            this.turtleToolStripMenuItem.Text = "Turtle";
            this.turtleToolStripMenuItem.Click += new System.EventHandler(this.turtleToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(972, 648);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupActions);
            this.Controls.Add(this.groupShapes);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.menuStrip1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "VectorEdit";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyUp);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbLine)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbRect)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbCirc)).EndInit();
            this.groupShapes.ResumeLayout(false);
            this.groupShapes.PerformLayout();
            this.groupActions.ResumeLayout(false);
            this.groupActions.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbMove)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbDraw)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbMovC)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbColor)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.PictureBox pbLine;
        private System.Windows.Forms.PictureBox pbRect;
        private System.Windows.Forms.PictureBox pbCirc;
        private System.Windows.Forms.GroupBox groupShapes;
        private System.Windows.Forms.GroupBox groupActions;
        private System.Windows.Forms.PictureBox pbMove;
        private System.Windows.Forms.PictureBox pbDraw;
        private System.Windows.Forms.PictureBox pbMovC;
        private System.Windows.Forms.PictureBox pbEdit;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.PictureBox pbColor;
        private System.Windows.Forms.Button buttonColor;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.CheckBox checkBoxGrid;
        private System.Windows.Forms.PictureBox pbGrid;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelCirc;
        private System.Windows.Forms.Label labelRect;
        private System.Windows.Forms.Label labelLine;
        private System.Windows.Forms.Label labelMovC;
        private System.Windows.Forms.Label labelMove;
        private System.Windows.Forms.Label labelEdit;
        private System.Windows.Forms.Label labelDraw;
        private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem selectAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deselectAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem resetCanvasPositionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem turtleToolStripMenuItem;
    }
}

