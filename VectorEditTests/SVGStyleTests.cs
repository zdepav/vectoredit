﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tests
{
    [TestClass()]
    public class SVGStyleTests
    {
        [TestMethod()]
        public void SVGStyleTest()
        {
            
        }

        [TestMethod()]
        public void TryGetNumberTest()
        {
            
        }

        [TestMethod()]
        public void TryGetColorTest()
        {
            GetColor("fill: #00ff7f;");
            GetColor(" fill  :  rgb ( 0, 255,    127) ;");
            GetColor("fill: rgba(0,255,127,1);");
            GetColor("fill: hsl(150, 100%, 50%);");
            GetColor("fill : hsla(    150 ,100   % , 50% ,1);");
        }

        private void GetColor(string s)
        {
            VectorEdit.SVGUtil.SVGStyle ss = new VectorEdit.SVGUtil.SVGStyle(s);
            Color c;
            if (ss.TryGetColor("fill", out c))
            {
                Assert.AreEqual(255, c.A, 0.1, "A is " + c.A);
                Assert.AreEqual(0, c.R, 0.1, "R is " + c.R);
                Assert.AreEqual(255, c.G, 0.1, "G is " + c.G);
                Assert.AreEqual(127, c.B, 0.1, "B is " + c.B);
            }
            else Assert.Fail();
        }

        [TestMethod()]
        public void ReadParamFloatsFromStringTest()
        {
            
        }
    }
}